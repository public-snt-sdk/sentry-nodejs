export interface StackFrame {
    fileName: string;
    lineNumber: number;
    functionName: string;
    typeName: string;
    methodName: string;
    native: boolean;
    columnNumber: number;
}
/** Extracts StackFrames from the Error */
export declare function parse(err: Error): StackFrame[];
//# sourceMappingURL=stacktrace.d.ts.map
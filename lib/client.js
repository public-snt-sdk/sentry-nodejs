Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("./core");
const backend_1 = require("./backend");
/**
 * The Sentry Node SDK Client.
 *
 * @see NodeOptions for documentation on configuration options.
 * @see SentryClient for usage documentation.
 */
class NodeClient extends core_1.BaseClient {
    /**
     * Creates a new Node SDK instance.
     * @param options Configuration options for this SDK.
     */
    constructor(options) {
        super(backend_1.NodeBackend, options);
    }
    /**
     * @inheritDoc
     */
    _prepareEvent(event, scope, hint) {
        event.platform = event.platform || 'node';
        if (this.getOptions().serverName) {
            event.server_name = this.getOptions().serverName;
        }
        return super._prepareEvent(event, scope, hint);
    }
}
exports.NodeClient = NodeClient;
//# sourceMappingURL=client.js.map
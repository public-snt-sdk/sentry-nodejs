Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("./core");
const types_1 = require("./types");
const utils_1 = require("./utils");
const parsers_1 = require("./parsers");
const transports_1 = require("./transports");
/**
 * The Sentry Node SDK Backend.
 * @hidden
 */
class NodeBackend extends core_1.BaseBackend {
    eventFromException(exception, hint) {
        let ex = exception;
        const mechanism = {
            handled: true,
            type: 'generic',
        };
        if (!utils_1.isError(exception)) {
            if (utils_1.isPlainObject(exception)) {
                // This will allow us to group events based on top-level keys
                // which is much better than creating new group when any key/value change
                const message = `Non-Error exception captured with keys: ${utils_1.extractExceptionKeysForMessage(exception)}`;
                core_1.getCurrentHub().configureScope((scope) => {
                    scope.setExtra('__serialized__', utils_1.normalizeToSize(exception));
                });
                ex = (hint && hint.syntheticException) || new Error(message);
                ex.message = message;
            }
            else {
                // This handles when someone does: `throw "something awesome";`
                // We use synthesized Error here so we can extract a (rough) stack trace.
                ex = (hint && hint.syntheticException) || new Error(exception);
                ex.message = exception;
            }
            mechanism.synthetic = true;
        }
        return new utils_1.SyncPromise((resolve, reject) => parsers_1.parseError(ex, this._options)
            .then(event => {
            utils_1.addExceptionTypeValue(event, undefined, undefined);
            utils_1.addExceptionMechanism(event, mechanism);
            resolve(Object.assign(Object.assign({}, event), { event_id: hint && hint.event_id }));
        })
            .then(null, reject));
    }
    /**
     * @inheritDoc
     */
    eventFromMessage(message, level = types_1.Severity.Info, hint) {
        const event = {
            event_id: hint && hint.event_id,
            level,
            message,
        };
        return new utils_1.SyncPromise(resolve => {
            if (this._options.attachStacktrace && hint && hint.syntheticException) {
                const stack = hint.syntheticException ? parsers_1.extractStackFromError(hint.syntheticException) : [];
                parsers_1.parseStack(stack, this._options)
                    .then(frames => {
                    event.stacktrace = {
                        frames: parsers_1.prepareFramesForEvent(frames),
                    };
                    resolve(event);
                })
                    .then(null, () => {
                    resolve(event);
                });
            }
            else {
                resolve(event);
            }
        });
    }
    /**
     * @inheritDoc
     */
    _setupTransport() {
        if (!this._options.dsn) {
            // We return the noop transport here in case there is no Dsn.
            return super._setupTransport();
        }
        const dsn = new utils_1.Dsn(this._options.dsn);
        const transportOptions = Object.assign(Object.assign(Object.assign(Object.assign(Object.assign({}, this._options.transportOptions), (this._options.httpProxy && { httpProxy: this._options.httpProxy })), (this._options.httpsProxy && { httpsProxy: this._options.httpsProxy })), (this._options.caCerts && { caCerts: this._options.caCerts })), { dsn: this._options.dsn });
        if (this._options.transport) {
            return new this._options.transport(transportOptions);
        }
        if (dsn.protocol === 'http') {
            return new transports_1.HTTPTransport(transportOptions);
        }
        return new transports_1.HTTPSTransport(transportOptions);
    }
}
exports.NodeBackend = NodeBackend;
//# sourceMappingURL=backend.js.map
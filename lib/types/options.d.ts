import { Breadcrumb, BreadcrumbHint } from './breadcrumb';
import { Event, EventHint } from './event';
import { Integration } from './integration';
import { LogLevel } from './loglevel';
import { SamplingContext } from './transaction';
import { Transport, TransportClass, TransportOptions } from './transport';
/** Base configuration options for every SDK. */
export interface Options {
    debug?: boolean;
    enabled?: boolean;
    dsn?: string;
    defaultIntegrations?: false | Integration[];
    integrations?: Integration[] | ((integrations: Integration[]) => Integration[]);
    ignoreErrors?: (string | RegExp)[];
    transport?: TransportClass<Transport>;
    transportOptions?: TransportOptions;
    release?: string;
    environment?: string;
    dist?: string;
    maxBreadcrumbs?: number;
    logLevel?: LogLevel;
    sampleRate?: number;
    attachStacktrace?: boolean;
    maxValueLength?: number;
    normalizeDepth?: number;
    shutdownTimeout?: number;
    tracesSampleRate?: number;
    _experiments?: {
        [key: string]: any;
    };
    tracesSampler?(samplingContext: SamplingContext): number | boolean;
    beforeSend?(event: Event, hint?: EventHint): PromiseLike<Event | null> | Event | null;
    beforeBreadcrumb?(breadcrumb: Breadcrumb, hint?: BreadcrumbHint): Breadcrumb | null;
}
//# sourceMappingURL=options.d.ts.map
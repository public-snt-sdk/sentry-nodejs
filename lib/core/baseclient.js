Object.defineProperty(exports, "__esModule", { value: true });
const hub_1 = require("../hub");
const types_1 = require("../types");
const utils_1 = require("../utils");
const integration_1 = require("./integration");
/**
 * Base implementation for all JavaScript SDK clients.
 *
 * Call the constructor with the corresponding backend constructor and options
 * specific to the client subclass. To access these options later, use
 * {@link Client.getOptions}. Also, the Backend instance is available via
 * {@link Client.getBackend}.
 *
 * If a Dsn is specified in the options, it will be parsed and stored. Use
 * {@link Client.getDsn} to retrieve the Dsn at any moment. In case the Dsn is
 * invalid, the constructor will throw a {@link SentryException}. Note that
 * without a valid Dsn, the SDK will not send any events to Sentry.
 *
 * Before sending an event via the backend, it is passed through
 * {@link BaseClient._prepareEvent} to add SDK information and scope data
 * (breadcrumbs and context). To add more custom information, override this
 * method and extend the resulting prepared event.
 *
 * To issue automatically created events (e.g. via instrumentation), use
 * {@link Client.captureEvent}. It will prepare the event and pass it through
 * the callback lifecycle. To issue auto-breadcrumbs, use
 * {@link Client.addBreadcrumb}.
 *
 * @example
 * class NodeClient extends BaseClient<NodeBackend, NodeOptions> {
 *   public constructor(options: NodeOptions) {
 *     super(NodeBackend, options);
 *   }
 *
 *   // ...
 * }
 */
class BaseClient {
    /**
     * Initializes this client instance.
     *
     * @param backendClass A constructor function to create the backend.
     * @param options Options for the client.
     */
    constructor(backendClass, options) {
        /** Array of used integrations. */
        this._integrations = {};
        /** Number of call being processed */
        this._processing = 0;
        this._backend = new backendClass(options);
        this._options = options;
        if (options.dsn) {
            this._dsn = new utils_1.Dsn(options.dsn);
        }
    }
    /**
     * @inheritDoc
     */
    captureException(exception, hint, scope) {
        let eventId = hint && hint.event_id;
        this._process(this._getBackend()
            .eventFromException(exception, hint)
            .then(event => this._captureEvent(event, hint, scope))
            .then(result => {
            eventId = result;
        }));
        return eventId;
    }
    /**
     * @inheritDoc
     */
    captureMessage(message, level, hint, scope) {
        let eventId = hint && hint.event_id;
        const promisedEvent = utils_1.isPrimitive(message)
            ? this._getBackend().eventFromMessage(String(message), level, hint)
            : this._getBackend().eventFromException(message, hint);
        this._process(promisedEvent
            .then(event => this._captureEvent(event, hint, scope))
            .then(result => {
            eventId = result;
        }));
        return eventId;
    }
    /**
     * @inheritDoc
     */
    captureEvent(event, hint, scope) {
        let eventId = hint && hint.event_id;
        this._process(this._captureEvent(event, hint, scope).then(result => {
            eventId = result;
        }));
        return eventId;
    }
    /**
     * @inheritDoc
     */
    captureSession(session) {
        if (!session.release) {
            utils_1.logger.warn('Discarded session because of missing release');
        }
        else {
            this._sendSession(session);
            // After sending, we set init false to inidcate it's not the first occurence
            session.update({ init: false });
        }
    }
    /**
     * @inheritDoc
     */
    getDsn() {
        return this._dsn;
    }
    /**
     * @inheritDoc
     */
    getOptions() {
        return this._options;
    }
    /**
     * @inheritDoc
     */
    flush(timeout) {
        return this._isClientProcessing(timeout).then(ready => {
            return this._getBackend()
                .getTransport()
                .close(timeout)
                .then(transportFlushed => ready && transportFlushed);
        });
    }
    /**
     * @inheritDoc
     */
    close(timeout) {
        return this.flush(timeout).then(result => {
            this.getOptions().enabled = false;
            return result;
        });
    }
    /**
     * Sets up the integrations
     */
    setupIntegrations() {
        if (this._isEnabled()) {
            this._integrations = integration_1.setupIntegrations(this._options);
        }
    }
    /**
     * @inheritDoc
     */
    getIntegration(integration) {
        try {
            return this._integrations[integration.id] || null;
        }
        catch (_oO) {
            utils_1.logger.warn(`Cannot retrieve integration ${integration.id} from the current Client`);
            return null;
        }
    }
    /** Updates existing session based on the provided event */
    _updateSessionFromEvent(session, event) {
        let crashed = false;
        let errored = false;
        let userAgent;
        const exceptions = event.exception && event.exception.values;
        if (exceptions) {
            errored = true;
            for (const ex of exceptions) {
                const mechanism = ex.mechanism;
                if (mechanism && mechanism.handled === false) {
                    crashed = true;
                    break;
                }
            }
        }
        const user = event.user;
        if (!session.userAgent) {
            const headers = event.request ? event.request.headers : {};
            for (const key in headers) {
                if (key.toLowerCase() === 'user-agent') {
                    userAgent = headers[key];
                    break;
                }
            }
        }
        session.update(Object.assign(Object.assign({}, (crashed && { status: types_1.SessionStatus.Crashed })), { user,
            userAgent, errors: session.errors + Number(errored || crashed) }));
        this.captureSession(session);
    }
    /** Deliver captured session to Sentry */
    _sendSession(session) {
        this._getBackend().sendSession(session);
    }
    /** Waits for the client to be done with processing. */
    _isClientProcessing(timeout) {
        return new utils_1.SyncPromise(resolve => {
            let ticked = 0;
            const tick = 1;
            const interval = setInterval(() => {
                if (this._processing === 0) {
                    clearInterval(interval);
                    resolve(true);
                }
                else {
                    ticked += tick;
                    if (timeout && ticked >= timeout) {
                        clearInterval(interval);
                        resolve(false);
                    }
                }
            }, tick);
        });
    }
    /** Returns the current backend. */
    _getBackend() {
        return this._backend;
    }
    /** Determines whether this SDK is enabled and a valid Dsn is present. */
    _isEnabled() {
        return this.getOptions().enabled !== false && this._dsn !== undefined;
    }
    /**
     * Adds common information to events.
     *
     * The information includes release and environment from `options`,
     * breadcrumbs and context (extra, tags and user) from the scope.
     *
     * Information that is already present in the event is never overwritten. For
     * nested objects, such as the context, keys are merged.
     *
     * @param event The original event.
     * @param hint May contain additional information about the original exception.
     * @param scope A scope containing event metadata.
     * @returns A new event with more information.
     */
    _prepareEvent(event, scope, hint) {
        const { normalizeDepth = 3 } = this.getOptions();
        const prepared = Object.assign(Object.assign({}, event), { event_id: event.event_id || (hint && hint.event_id ? hint.event_id : utils_1.uuid4()), timestamp: event.timestamp || utils_1.dateTimestampInSeconds() });
        this._applyClientOptions(prepared);
        let finalScope = scope;
        if (hint && hint.captureContext) {
            finalScope = hub_1.Scope.clone(finalScope).update(hint.captureContext);
        }
        // We prepare the result here with a resolved Event.
        let result = utils_1.SyncPromise.resolve(prepared);
        // This should be the last thing called, since we want that
        // {@link Hub.addEventProcessor} gets the finished prepared event.
        if (finalScope) {
            // In case we have a hub we reassign it.
            result = finalScope.applyToEvent(prepared, hint);
        }
        return result.then(evt => {
            if (typeof normalizeDepth === 'number' && normalizeDepth > 0) {
                return this._normalizeEvent(evt, normalizeDepth);
            }
            return evt;
        });
    }
    _normalizeEvent(event, depth) {
        if (!event) {
            return null;
        }
        const normalized = Object.assign(Object.assign(Object.assign(Object.assign(Object.assign({}, event), (event.breadcrumbs && {
            breadcrumbs: event.breadcrumbs.map(b => (Object.assign(Object.assign({}, b), (b.data && {
                data: utils_1.normalize(b.data, depth),
            })))),
        })), (event.user && {
            user: utils_1.normalize(event.user, depth),
        })), (event.contexts && {
            contexts: utils_1.normalize(event.contexts, depth),
        })), (event.extra && {
            extra: utils_1.normalize(event.extra, depth),
        }));
        // event.contexts.trace stores information about a Transaction. Similarly,
        // event.spans[] stores information about child Spans. Given that a
        // Transaction is conceptually a Span, normalization should apply to both
        // Transactions and Spans consistently.
        // For now the decision is to skip normalization of Transactions and Spans,
        // so this block overwrites the normalized event to add back the original
        // Transaction information prior to normalization.
        if (event.contexts && event.contexts.trace) {
            normalized.contexts.trace = event.contexts.trace;
        }
        return normalized;
    }
    /**
     *  Enhances event using the client configuration.
     *  It takes care of all "static" values like environment, release and `dist`,
     *  as well as truncating overly long values.
     * @param event event instance to be enhanced
     */
    _applyClientOptions(event) {
        const options = this.getOptions();
        const { environment, release, dist, maxValueLength = 250 } = options;
        if (!('environment' in event)) {
            event.environment = 'environment' in options ? environment : 'production';
        }
        if (event.release === undefined && release !== undefined) {
            event.release = release;
        }
        if (event.dist === undefined && dist !== undefined) {
            event.dist = dist;
        }
        if (event.message) {
            event.message = utils_1.truncate(event.message, maxValueLength);
        }
        const exception = event.exception && event.exception.values && event.exception.values[0];
        if (exception && exception.value) {
            exception.value = utils_1.truncate(exception.value, maxValueLength);
        }
        const request = event.request;
        if (request && request.url) {
            request.url = utils_1.truncate(request.url, maxValueLength);
        }
    }
    _sendEvent(event) {
        this._getBackend().sendEvent(event);
    }
    /**
     * Processes the event and logs an error in case of rejection
     * @param event
     * @param hint
     * @param scope
     */
    _captureEvent(event, hint, scope) {
        return this._processEvent(event, hint, scope).then(finalEvent => {
            return finalEvent.event_id;
        }, reason => {
            utils_1.logger.error(reason);
            return undefined;
        });
    }
    /**
     * Processes an event (either error or message) and sends it to Sentry.
     *
     * This also adds breadcrumbs and context information to the event. However,
     * platform specific meta data (such as the User's IP address) must be added
     * by the SDK implementor.
     *
     *
     * @param event The event to send to Sentry.
     * @param hint May contain additional information about the original exception.
     * @param scope A scope containing event metadata.
     * @returns A SyncPromise that resolves with the event or rejects in case event was/will not be send.
     */
    _processEvent(event, hint, scope) {
        const { beforeSend, sampleRate } = this.getOptions();
        if (!this._isEnabled()) {
            return utils_1.SyncPromise.reject(new utils_1.SentryError('SDK not enabled, will not send event.'));
        }
        const isTransaction = event.type === 'transaction';
        // 1.0 === 100% events are sent
        // 0.0 === 0% events are sent
        // Sampling for transaction happens somewhere else
        if (!isTransaction && typeof sampleRate === 'number' && Math.random() > sampleRate) {
            return utils_1.SyncPromise.reject(new utils_1.SentryError(`Discarding event because it's not included in the random sample (sampling rate = ${sampleRate})`));
        }
        return this._prepareEvent(event, scope, hint)
            .then(prepared => {
            if (prepared === null) {
                throw new utils_1.SentryError('An event processor returned null, will not send event.');
            }
            const isInternalException = hint && hint.data && hint.data.__sentry__ === true;
            if (isInternalException || isTransaction || !beforeSend) {
                return prepared;
            }
            const beforeSendResult = beforeSend(prepared, hint);
            if (typeof beforeSendResult === 'undefined') {
                throw new utils_1.SentryError('`beforeSend` method has to return `null` or a valid event.');
            }
            if (utils_1.isThenable(beforeSendResult)) {
                return beforeSendResult.then(
                // tslint:disable-next-line: no-shadowed-variable
                event => event, e => {
                    throw new utils_1.SentryError(`beforeSend rejected with ${e}`);
                });
            }
            return beforeSendResult;
        })
            .then(processedEvent => {
            if (processedEvent === null) {
                throw new utils_1.SentryError('`beforeSend` returned `null`, will not send event.');
            }
            const session = scope && scope.getSession && scope.getSession();
            if (!isTransaction && session) {
                this._updateSessionFromEvent(session, processedEvent);
            }
            this._sendEvent(processedEvent);
            return processedEvent;
        })
            .then(null, reason => {
            if (reason instanceof utils_1.SentryError) {
                throw reason;
            }
            this.captureException(reason, {
                data: {
                    __sentry__: true,
                },
                originalException: reason,
            });
            throw new utils_1.SentryError(`Event processing pipeline threw an error, original event will not be sent. Details have been sent as a new event.\nReason: ${reason}`);
        });
    }
    /**
     * Occupies the client with processing and event
     */
    _process(promise) {
        this._processing += 1;
        promise.then(value => {
            this._processing -= 1;
            return value;
        }, reason => {
            this._processing -= 1;
            return reason;
        });
    }
}
exports.BaseClient = BaseClient;
//# sourceMappingURL=baseclient.js.map
Object.defineProperty(exports, "__esModule", { value: true });
const noop_1 = require("./transports/noop");
const utils_1 = require("../utils");
/**
 * This is the base implemention of a Backend.
 * @hidden
 */
class BaseBackend {
    /** Creates a new backend instance. */
    constructor(options) {
        this._options = options;
        if (!this._options.dsn) {
            utils_1.logger.warn('No DSN provided, backend will not do anything.');
        }
        this._transport = this._setupTransport();
    }
    /**
     * @inheritDoc
     */
    eventFromException(_exception, _hint) {
        throw new utils_1.SentryError('Backend has to implement `eventFromException` method');
    }
    /**
     * @inheritDoc
     */
    eventFromMessage(_message, _level, _hint) {
        throw new utils_1.SentryError('Backend has to implement `eventFromMessage` method');
    }
    /**
     * @inheritDoc
     */
    sendEvent(event) {
        this._transport.sendEvent(event).then(null, reason => {
            utils_1.logger.error(`Error while sending event: ${reason}`);
        });
    }
    /**
     * @inheritDoc
     */
    sendSession(session) {
        if (!this._transport.sendSession) {
            utils_1.logger.warn('Dropping session because custom transport doesn\'t implement sendSession');
            return;
        }
        this._transport.sendSession(session).then(null, reason => {
            utils_1.logger.error(`Error while sending session: ${reason}`);
        });
    }
    /**
     * @inheritDoc
     */
    getTransport() {
        return this._transport;
    }
    /**
     * Sets up the transport so it can be used later to send requests.
     */
    _setupTransport() {
        return new noop_1.NoopTransport();
    }
}
exports.BaseBackend = BaseBackend;
//# sourceMappingURL=basebackend.js.map
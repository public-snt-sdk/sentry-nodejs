Object.defineProperty(exports, "__esModule", { value: true });
const hub_1 = require("../hub");
const utils_1 = require("../utils");
exports.installedIntegrations = [];
/** Gets integration to install */
function getIntegrationsToSetup(options) {
    const defaultIntegrations = (options.defaultIntegrations && [...options.defaultIntegrations]) || [];
    const userIntegrations = options.integrations;
    let integrations = [];
    if (Array.isArray(userIntegrations)) {
        const userIntegrationsNames = userIntegrations.map(i => i.name);
        const pickedIntegrationsNames = [];
        // Leave only unique default integrations, that were not overridden with provided user integrations
        defaultIntegrations.forEach(defaultIntegration => {
            if (userIntegrationsNames.indexOf(defaultIntegration.name) === -1 &&
                pickedIntegrationsNames.indexOf(defaultIntegration.name) === -1) {
                integrations.push(defaultIntegration);
                pickedIntegrationsNames.push(defaultIntegration.name);
            }
        });
        // Don't add same user integration twice
        userIntegrations.forEach(userIntegration => {
            if (pickedIntegrationsNames.indexOf(userIntegration.name) === -1) {
                integrations.push(userIntegration);
                pickedIntegrationsNames.push(userIntegration.name);
            }
        });
    }
    else if (typeof userIntegrations === 'function') {
        integrations = userIntegrations(defaultIntegrations);
        integrations = Array.isArray(integrations) ? integrations : [integrations];
    }
    else {
        integrations = [...defaultIntegrations];
    }
    // Make sure that if present, `Debug` integration will always run last
    const integrationsNames = integrations.map(i => i.name);
    const alwaysLastToRun = 'Debug';
    if (integrationsNames.indexOf(alwaysLastToRun) !== -1) {
        integrations.push(...integrations.splice(integrationsNames.indexOf(alwaysLastToRun), 1));
    }
    return integrations;
}
exports.getIntegrationsToSetup = getIntegrationsToSetup;
/** Setup given integration */
function setupIntegration(integration) {
    if (exports.installedIntegrations.indexOf(integration.name) !== -1) {
        return;
    }
    integration.setupOnce(hub_1.addGlobalEventProcessor, hub_1.getCurrentHub);
    exports.installedIntegrations.push(integration.name);
    utils_1.logger.log(`Integration installed: ${integration.name}`);
}
exports.setupIntegration = setupIntegration;
/**
 * Given a list of integration instances this installs them all. When `withDefaults` is set to `true` then all default
 * integrations are added unless they were already provided before.
 * @param integrations array of integration instances
 * @param withDefault should enable default integrations
 */
function setupIntegrations(options) {
    const integrations = {};
    getIntegrationsToSetup(options).forEach(integration => {
        integrations[integration.name] = integration;
        setupIntegration(integration);
    });
    return integrations;
}
exports.setupIntegrations = setupIntegrations;
//# sourceMappingURL=integration.js.map
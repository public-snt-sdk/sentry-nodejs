Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
/** Creates a SentryRequest from an event. */
function eventToSentryRequest(event, api) {
    const eventType = event.type || 'event';
    const _a = event.debug_meta || {}, { transactionSampling } = _a, metadata = tslib_1.__rest(_a, ["transactionSampling"]);
    if (Object.keys(metadata).length === 0) {
        delete event.debug_meta;
    }
    else {
        event.debug_meta = metadata;
    }
    const req = {
        body: JSON.stringify(event),
        type: eventType,
        url: api.getEndpointWithUrlEncodedAuth(),
    };
    return req;
}
exports.eventToSentryRequest = eventToSentryRequest;
//# sourceMappingURL=request.js.map
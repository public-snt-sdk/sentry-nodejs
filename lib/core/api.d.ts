import { DsnLike } from '../types';
import { Dsn } from '../utils';
export declare class API {
    /** The DSN as passed to Sentry.init() */
    dsn: DsnLike;
    /** The internally used Dsn object. */
    private readonly _dsnObject;
    /** Create a new instance of API */
    constructor(dsn: DsnLike);
    /** Returns the Dsn object. */
    getDsn(): Dsn;
    /** Returns the prefix to construct Sentry ingestion API endpoints. */
    getBaseApiEndpoint(): string;
    /** Returns the store endpoint URL. */
    getStoreEndpoint(): string;
    getEndpointWithUrlEncodedAuth(): string;
    /**
     * Returns an object that can be used in request headers.
     * This is needed for node and the old /store endpoint in sentry
     */
    getRequestHeaders(): {
        [key: string]: string;
    };
    getReportDialogEndpoint(dialogOptions?: {
        [key: string]: any;
        user?: {
            name?: string;
            email?: string;
        };
    }): string;
    /** Returns the envelope endpoint URL. */
    getEndpoint(): string;
    /** Returns the ingest API endpoint for target. */
    private _getIngestEndpoint;
    /** Returns a URL-encoded string with auth config suitable for a query string. */
    private _encodedAuth;
}
//# sourceMappingURL=api.d.ts.map
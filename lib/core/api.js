Object.defineProperty(exports, "__esModule", { value: true });
const utils_1 = require("../utils");
class API {
    /** Create a new instance of API */
    constructor(dsn) {
        this.dsn = dsn;
        this._dsnObject = new utils_1.Dsn(dsn);
    }
    /** Returns the Dsn object. */
    getDsn() {
        return this._dsnObject;
    }
    /** Returns the prefix to construct Sentry ingestion API endpoints. */
    getBaseApiEndpoint() {
        const dsn = this._dsnObject;
        const protocol = dsn.protocol ? `${dsn.protocol}:` : '';
        const port = dsn.port ? `:${dsn.port}` : '';
        return `${protocol}//${dsn.host}${port}${dsn.path ? `/${dsn.path}` : ''}/`;
    }
    /** Returns the store endpoint URL. */
    getStoreEndpoint() {
        return this._getIngestEndpoint();
    }
    getEndpointWithUrlEncodedAuth() {
        return `${this.getEndpoint()}?${this._encodedAuth()}`;
    }
    /**
     * Returns an object that can be used in request headers.
     * This is needed for node and the old /store endpoint in sentry
     */
    getRequestHeaders() {
        // CHANGE THIS to use metadata but keep clientName and clientVersion compatible
        const dsn = this._dsnObject;
        if (!dsn.publicKey || !dsn.pass) {
            throw new Error('Authorization');
        }
        const headers = {
            'Content-Type': 'application/json',
            'sentry_apikey': `${dsn.publicKey}-${dsn.pass.toString()}`,
        };
        return headers;
    }
    getReportDialogEndpoint(dialogOptions = {}) {
        const dsn = this._dsnObject;
        const endpoint = `${this.getBaseApiEndpoint()}embed/error-page/`;
        const encodedOptions = [];
        encodedOptions.push(`dsn=${dsn.toString()}`);
        for (const key in dialogOptions) {
            if (key === 'dsn') {
                continue;
            }
            if (key === 'user') {
                if (!dialogOptions.user) {
                    continue;
                }
                if (dialogOptions.user.name) {
                    encodedOptions.push(`name=${encodeURIComponent(dialogOptions.user.name)}`);
                }
                if (dialogOptions.user.email) {
                    encodedOptions.push(`email=${encodeURIComponent(dialogOptions.user.email)}`);
                }
            }
            else {
                encodedOptions.push(`${encodeURIComponent(key)}=${encodeURIComponent(dialogOptions[key])}`);
            }
        }
        if (encodedOptions.length) {
            return `${endpoint}?${encodedOptions.join('&')}`;
        }
        return endpoint;
    }
    /** Returns the envelope endpoint URL. */
    getEndpoint() {
        return this._getIngestEndpoint();
    }
    /** Returns the ingest API endpoint for target. */
    _getIngestEndpoint() {
        const base = this.getBaseApiEndpoint();
        const dsn = this._dsnObject;
        return `${base}${dsn.projectId}/eventLogs/`;
    }
    /** Returns a URL-encoded string with auth config suitable for a query string. */
    _encodedAuth() {
        const dsn = this._dsnObject;
        const auth = {
            [dsn.publicKey]: dsn.pass,
        };
        return utils_1.urlEncode(auth);
    }
}
exports.API = API;
//# sourceMappingURL=api.js.map
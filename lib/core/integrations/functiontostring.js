Object.defineProperty(exports, "__esModule", { value: true });
let originalFunctionToString;
/** Patch toString calls to return proper name for wrapped functions */
class FunctionToString {
    constructor() {
        /**
         * @inheritDoc
         */
        this.name = FunctionToString.id;
    }
    /**
     * @inheritDoc
     */
    setupOnce() {
        originalFunctionToString = Function.prototype.toString;
        Function.prototype.toString = function (...args) {
            const context = this.__sentry_original__ || this;
            return originalFunctionToString.apply(context, args);
        };
    }
}
exports.FunctionToString = FunctionToString;
/**
 * @inheritDoc
 */
FunctionToString.id = 'FunctionToString';
//# sourceMappingURL=functiontostring.js.map
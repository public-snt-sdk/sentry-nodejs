Object.defineProperty(exports, "__esModule", { value: true });
const hub_1 = require("../../hub");
const utils_1 = require("../../utils");
// "Script error." is hard coded into browsers for errors that it can't read.
// this is the result of a script being pulled in from an external domain and CORS.
const DEFAULT_IGNORE_ERRORS = [/^Script error\.?$/, /^Javascript error: Script error\.? on line 0$/];
/** Inbound filters configurable by the user */
class InboundFilters {
    constructor(_options = {}) {
        this._options = _options;
        /**
         * @inheritDoc
         */
        this.name = InboundFilters.id;
    }
    /**
     * @inheritDoc
     */
    setupOnce() {
        hub_1.addGlobalEventProcessor((event) => {
            const hub = hub_1.getCurrentHub();
            if (!hub) {
                return event;
            }
            const self = hub.getIntegration(InboundFilters);
            if (self) {
                const client = hub.getClient();
                const clientOptions = client ? client.getOptions() : {};
                const options = self._mergeOptions(clientOptions);
                if (self._shouldDropEvent(event, options)) {
                    return null;
                }
            }
            return event;
        });
    }
    /** JSDoc */
    _shouldDropEvent(event, options) {
        if (this._isSentryError(event, options)) {
            utils_1.logger.warn(`Event dropped due to being internal Sentry Error.\nEvent: ${utils_1.getEventDescription(event)}`);
            return true;
        }
        if (this._isIgnoredError(event, options)) {
            utils_1.logger.warn(`Event dropped due to being matched by \`ignoreErrors\` option.\nEvent: ${utils_1.getEventDescription(event)}`);
            return true;
        }
        if (this._isDeniedUrl(event, options)) {
            utils_1.logger.warn(`Event dropped due to being matched by \`denyUrls\` option.\nEvent: ${utils_1.getEventDescription(event)}.\nUrl: ${this._getEventFilterUrl(event)}`);
            return true;
        }
        if (!this._isAllowedUrl(event, options)) {
            utils_1.logger.warn(`Event dropped due to not being matched by \`allowUrls\` option.\nEvent: ${utils_1.getEventDescription(event)}.\nUrl: ${this._getEventFilterUrl(event)}`);
            return true;
        }
        return false;
    }
    /** JSDoc */
    _isSentryError(event, options) {
        if (!options.ignoreInternal) {
            return false;
        }
        try {
            return ((event &&
                event.exception &&
                event.exception.values &&
                event.exception.values[0] &&
                event.exception.values[0].type === 'SentryError') ||
                false);
        }
        catch (_oO) {
            return false;
        }
    }
    /** JSDoc */
    _isIgnoredError(event, options) {
        if (!options.ignoreErrors || !options.ignoreErrors.length) {
            return false;
        }
        return this._getPossibleEventMessages(event).some(message => 
        // Not sure why TypeScript complains here...
        options.ignoreErrors.some(pattern => utils_1.isMatchingPattern(message, pattern)));
    }
    /** JSDoc */
    _isDeniedUrl(event, options) {
        // TODO: Use Glob instead?
        if (!options.denyUrls || !options.denyUrls.length) {
            return false;
        }
        const url = this._getEventFilterUrl(event);
        return !url ? false : options.denyUrls.some(pattern => utils_1.isMatchingPattern(url, pattern));
    }
    /** JSDoc */
    _isAllowedUrl(event, options) {
        // TODO: Use Glob instead?
        if (!options.allowUrls || !options.allowUrls.length) {
            return true;
        }
        const url = this._getEventFilterUrl(event);
        return !url ? true : options.allowUrls.some(pattern => utils_1.isMatchingPattern(url, pattern));
    }
    /** JSDoc */
    _mergeOptions(clientOptions = {}) {
        return {
            allowUrls: [
                ...(this._options.whitelistUrls || []),
                ...(this._options.allowUrls || []),
                ...(clientOptions.whitelistUrls || []),
                ...(clientOptions.allowUrls || []),
            ],
            denyUrls: [
                ...(this._options.blacklistUrls || []),
                ...(this._options.denyUrls || []),
                ...(clientOptions.blacklistUrls || []),
                ...(clientOptions.denyUrls || []),
            ],
            ignoreErrors: [
                ...(this._options.ignoreErrors || []),
                ...(clientOptions.ignoreErrors || []),
                ...DEFAULT_IGNORE_ERRORS,
            ],
            ignoreInternal: typeof this._options.ignoreInternal !== 'undefined' ? this._options.ignoreInternal : true,
        };
    }
    /** JSDoc */
    _getPossibleEventMessages(event) {
        if (event.message) {
            return [event.message];
        }
        if (event.exception) {
            try {
                const { type = '', value = '' } = (event.exception.values && event.exception.values[0]) || {};
                return [`${value}`, `${type}: ${value}`];
            }
            catch (oO) {
                utils_1.logger.error(`Cannot extract message for event ${utils_1.getEventDescription(event)}`);
                return [];
            }
        }
        return [];
    }
    /** JSDoc */
    _getEventFilterUrl(event) {
        try {
            if (event.stacktrace) {
                const frames = event.stacktrace.frames;
                return (frames && frames[frames.length - 1].filename) || null;
            }
            if (event.exception) {
                const frames = event.exception.values && event.exception.values[0].stacktrace && event.exception.values[0].stacktrace.frames;
                return (frames && frames[frames.length - 1].filename) || null;
            }
            return null;
        }
        catch (oO) {
            utils_1.logger.error(`Cannot extract url for event ${utils_1.getEventDescription(event)}`);
            return null;
        }
    }
}
exports.InboundFilters = InboundFilters;
/**
 * @inheritDoc
 */
InboundFilters.id = 'InboundFilters';
//# sourceMappingURL=inboundfilters.js.map
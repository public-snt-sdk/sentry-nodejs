Object.defineProperty(exports, "__esModule", { value: true });
const types_1 = require("../../types");
const utils_1 = require("../../utils");
/** Noop transport */
class NoopTransport {
    /**
     * @inheritDoc
     */
    sendEvent(_) {
        return utils_1.SyncPromise.resolve({
            reason: `NoopTransport: Event has been skipped because no Dsn is configured.`,
            status: types_1.Status.Skipped,
        });
    }
    /**
     * @inheritDoc
     */
    close(_) {
        return utils_1.SyncPromise.resolve(true);
    }
}
exports.NoopTransport = NoopTransport;
//# sourceMappingURL=noop.js.map
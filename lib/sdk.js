Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const core_1 = require("./core");
const hub_1 = require("./hub");
const utils_1 = require("./utils");
const domain = require("domain");
const client_1 = require("./client");
const integrations_1 = require("./integrations");
exports.defaultIntegrations = [
    // Common
    new core_1.Integrations.InboundFilters(),
    new core_1.Integrations.FunctionToString(),
    // Native Wrappers
    new integrations_1.Console(),
    new integrations_1.Http(),
    // Global Handlers
    new integrations_1.OnUncaughtException(),
    new integrations_1.OnUnhandledRejection(),
    // Misc
    new integrations_1.LinkedErrors(),
];
/**
 * The Sentry Node SDK Client.
 *
 * To use this SDK, call the {@link init} function as early as possible in the
 * main entry module. To set context information or send manual events, use the
 * provided methods.
 *
 * @example
 * ```
 *
 * const { init } = require('./node');
 *
 * init({
 *   dsn: '__DSN__',
 *   // ...
 * });
 * ```
 *
 * @example
 * ```
 *
 * const { configureScope } = require('@sentry/node');
 * configureScope((scope: Scope) => {
 *   scope.setExtra({ battery: 0.7 });
 *   scope.setTag({ user_mode: 'admin' });
 *   scope.setUser({ id: '4711' });
 * });
 * ```
 *
 * @example
 * ```
 *
 * const { addBreadcrumb } = require('@sentry/node');
 * addBreadcrumb({
 *   message: 'My Breadcrumb',
 *   // ...
 * });
 * ```
 *
 * @example
 * ```
 *
 * const Sentry = require('@sentry/node');
 * Sentry.captureMessage('Hello, world!');
 * Sentry.captureException(new Error('Good bye'));
 * Sentry.captureEvent({
 *   message: 'Manual',
 *   stacktrace: [
 *     // ...
 *   ],
 * });
 * ```
 *
 * @see {@link NodeOptions} for documentation on configuration options.
 */
function init(options = {}) {
    if (options.defaultIntegrations === undefined) {
        options.defaultIntegrations = exports.defaultIntegrations;
    }
    if (options.dsn === undefined && process.env.SENTRY_DSN) {
        options.dsn = process.env.SENTRY_DSN;
    }
    if (options.tracesSampleRate === undefined && process.env.SENTRY_TRACES_SAMPLE_RATE) {
        const tracesSampleRate = parseFloat(process.env.SENTRY_TRACES_SAMPLE_RATE);
        if (isFinite(tracesSampleRate)) {
            options.tracesSampleRate = tracesSampleRate;
        }
    }
    if (options.release === undefined) {
        const global = utils_1.getGlobalObject();
        // Prefer env var over global
        if (process.env.SENTRY_RELEASE) {
            options.release = process.env.SENTRY_RELEASE;
        }
        // This supports the variable that sentry-webpack-plugin injects
        else if (global.SENTRY_RELEASE && global.SENTRY_RELEASE.id) {
            options.release = global.SENTRY_RELEASE.id;
        }
    }
    if (options.environment === undefined && process.env.SENTRY_ENVIRONMENT) {
        options.environment = process.env.SENTRY_ENVIRONMENT;
    }
    if (domain.active) {
        hub_1.setHubOnCarrier(hub_1.getMainCarrier(), core_1.getCurrentHub());
    }
    core_1.initAndBind(client_1.NodeClient, options);
}
exports.init = init;
/**
 * This is the getter for lastEventId.
 *
 * @returns The last event id of a captured event.
 */
function lastEventId() {
    return core_1.getCurrentHub().lastEventId();
}
exports.lastEventId = lastEventId;
/**
 * A promise that resolves when all current events have been sent.
 * If you provide a timeout and the queue takes longer to drain the promise returns false.
 *
 * @param timeout Maximum time in ms the client should wait.
 */
function flush(timeout) {
    return tslib_1.__awaiter(this, void 0, void 0, function* () {
        const client = core_1.getCurrentHub().getClient();
        if (client) {
            return client.flush(timeout);
        }
        return Promise.reject(false);
    });
}
exports.flush = flush;
/**
 * A promise that resolves when all current events have been sent.
 * If you provide a timeout and the queue takes longer to drain the promise returns false.
 *
 * @param timeout Maximum time in ms the client should wait.
 */
function close(timeout) {
    return tslib_1.__awaiter(this, void 0, void 0, function* () {
        const client = core_1.getCurrentHub().getClient();
        if (client) {
            return client.close(timeout);
        }
        return Promise.reject(false);
    });
}
exports.close = close;
//# sourceMappingURL=sdk.js.map
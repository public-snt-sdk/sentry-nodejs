Object.defineProperty(exports, "__esModule", { value: true });
const utils_1 = require("../utils");
const https = require("https");
const base_1 = require("./base");
/** Node https module transport */
class HTTPSTransport extends base_1.BaseTransport {
    /** Create a new instance and set this.agent */
    constructor(options) {
        super(options);
        this.options = options;
        const proxy = options.httpsProxy || options.httpProxy || process.env.https_proxy || process.env.http_proxy;
        this.module = https;
        this.client = proxy
            ? new (require('https-proxy-agent'))(proxy)
            : new https.Agent({ keepAlive: false, maxSockets: 30, timeout: 2000 });
    }
    /**
     * @inheritDoc
     */
    sendEvent(event) {
        if (!this.module) {
            throw new utils_1.SentryError('No module available in HTTPSTransport');
        }
        return this._sendWithModule(this.module, event);
    }
}
exports.HTTPSTransport = HTTPSTransport;
//# sourceMappingURL=https.js.map
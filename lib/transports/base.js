Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const core_1 = require("../core");
const types_1 = require("../types");
const utils_1 = require("../utils");
const fs = require("fs");
const url = require("url");
const index_1 = require("../core/index");
/** Base Transport class implementation */
class BaseTransport {
    /** Create instance and set this.dsn */
    constructor(options) {
        this.options = options;
        /** A simple buffer holding all requests. */
        this._buffer = new utils_1.PromiseBuffer(30);
        /** Locks transport after receiving 429 response */
        this._disabledUntil = new Date(Date.now());
        this._api = new core_1.API(options.dsn);
    }
    /**
     * @inheritDoc
     */
    sendEvent(_) {
        throw new utils_1.SentryError('Transport Class has to implement `sendEvent` method.');
    }
    /**
     * @inheritDoc
     */
    close(timeout) {
        return this._buffer.drain(timeout);
    }
    /** Returns a build request option object used by request */
    _getRequestOptions(uri) {
        const headers = Object.assign({}, this._api.getRequestHeaders());
        const { hostname, pathname, port, protocol } = uri;
        const path = `${pathname}`;
        return Object.assign({ agent: this.client, headers,
            hostname, method: 'POST', path,
            port,
            protocol }, (this.options.caCerts && {
            ca: fs.readFileSync(this.options.caCerts),
        }));
    }
    /** JSDoc */
    _sendWithModule(httpModule, event) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            if (new Date(Date.now()) < this._disabledUntil) {
                return Promise.reject(new utils_1.SentryError(`Transport locked till ${this._disabledUntil} due to too many requests.`));
            }
            if (!this._buffer.isReady()) {
                return Promise.reject(new utils_1.SentryError('Not adding Promise due to buffer limit reached.'));
            }
            return this._buffer.add(new Promise((resolve, reject) => {
                const sentryReq = index_1.eventToSentryRequest(event, this._api);
                const options = this._getRequestOptions(new url.URL(sentryReq.url));
                const req = httpModule.request(options, (res) => {
                    const statusCode = res.statusCode || 500;
                    const status = types_1.Status.fromHttpCode(statusCode);
                    res.setEncoding('utf8');
                    if (status === types_1.Status.Success) {
                        resolve({ status });
                    }
                    else {
                        if (status === types_1.Status.RateLimit) {
                            const now = Date.now();
                            /**
                             * "Key-value pairs of header names and values. Header names are lower-cased."
                             * https://nodejs.org/api/http.html#http_message_headers
                             */
                            let retryAfterHeader = res.headers ? res.headers['retry-after'] : '';
                            retryAfterHeader = (Array.isArray(retryAfterHeader) ? retryAfterHeader[0] : retryAfterHeader);
                            this._disabledUntil = new Date(now + utils_1.parseRetryAfterHeader(now, retryAfterHeader));
                            utils_1.logger.warn(`Too many requests, backing off till: ${this._disabledUntil}`);
                        }
                        let rejectionMessage = `HTTP Error (${statusCode})`;
                        if (res.headers && res.headers['x-s`entry-error']) {
                            rejectionMessage += `: ${res.headers['x-sentry-error']}`;
                        }
                        reject(new utils_1.SentryError(rejectionMessage));
                    }
                    // Force the socket to drain
                    res.on('data', () => {
                        // Drain
                    });
                    res.on('end', () => {
                        // Drain
                    });
                });
                req.on('error', reject);
                req.end(sentryReq.body);
            }));
        });
    }
}
exports.BaseTransport = BaseTransport;
//# sourceMappingURL=base.js.map
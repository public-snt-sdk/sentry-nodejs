Object.defineProperty(exports, "__esModule", { value: true });
const utils_1 = require("../utils");
const http = require("http");
const base_1 = require("./base");
/** Node http module transport */
class HTTPTransport extends base_1.BaseTransport {
    /** Create a new instance and set this.agent */
    constructor(options) {
        super(options);
        this.options = options;
        const proxy = options.httpProxy || process.env.http_proxy;
        this.module = http;
        this.client = proxy
            ? new (require('https-proxy-agent'))(proxy)
            : new http.Agent({ keepAlive: false, maxSockets: 30, timeout: 2000 });
    }
    /**
     * @inheritDoc
     */
    sendEvent(event) {
        if (!this.module) {
            throw new utils_1.SentryError('No module available in HTTPTransport');
        }
        return this._sendWithModule(this.module, event);
    }
}
exports.HTTPTransport = HTTPTransport;
//# sourceMappingURL=http.js.map
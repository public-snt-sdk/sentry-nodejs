Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("../core");
const types_1 = require("../types");
const utils_1 = require("../utils");
const util = require("util");
/** Console module integration */
class Console {
    constructor() {
        /**
         * @inheritDoc
         */
        this.name = Console.id;
    }
    /**
     * @inheritDoc
     */
    setupOnce() {
        const consoleModule = require('console');
        for (const level of ['debug', 'info', 'warn', 'error', 'log']) {
            utils_1.fill(consoleModule, level, createConsoleWrapper(level));
        }
    }
}
exports.Console = Console;
/**
 * @inheritDoc
 */
Console.id = 'Console';
/**
 * Wrapper function that'll be used for every console level
 */
function createConsoleWrapper(level) {
    return function consoleWrapper(originalConsoleMethod) {
        let sentryLevel;
        switch (level) {
            case 'debug':
                sentryLevel = types_1.Severity.Debug;
                break;
            case 'error':
                sentryLevel = types_1.Severity.Error;
                break;
            case 'info':
                sentryLevel = types_1.Severity.Info;
                break;
            case 'warn':
                sentryLevel = types_1.Severity.Warning;
                break;
            default:
                sentryLevel = types_1.Severity.Log;
        }
        return function () {
            if (core_1.getCurrentHub().getIntegration(Console)) {
                core_1.getCurrentHub().addBreadcrumb({
                    category: 'console',
                    level: sentryLevel,
                    message: util.format.apply(undefined, arguments),
                }, {
                    input: arguments,
                    level,
                });
            }
            originalConsoleMethod.apply(this, arguments);
        };
    };
}
//# sourceMappingURL=console.js.map
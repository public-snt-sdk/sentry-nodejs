Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("../core");
const utils_1 = require("../utils");
const handlers_1 = require("../handlers");
/** Global Promise Rejection handler */
class OnUnhandledRejection {
    /**
     * @inheritDoc
     */
    constructor(_options = { mode: 'warn' }) {
        this._options = _options;
        /**
         * @inheritDoc
         */
        this.name = OnUnhandledRejection.id;
    }
    /**
     * @inheritDoc
     */
    setupOnce() {
        global.process.on('unhandledRejection', this.sendUnhandledPromise.bind(this));
    }
    /**
     * Send an exception with reason
     * @param reason string
     * @param promise promise
     */
    sendUnhandledPromise(reason, promise) {
        const hub = core_1.getCurrentHub();
        if (!hub.getIntegration(OnUnhandledRejection)) {
            this._handleRejection(reason);
            return;
        }
        const context = (promise.domain && promise.domain.sentryContext) || {};
        hub.withScope((scope) => {
            scope.setExtra('unhandledPromiseRejection', true);
            // Preserve backwards compatibility with raven-node for now
            if (context.user) {
                scope.setUser(context.user);
            }
            if (context.tags) {
                scope.setTags(context.tags);
            }
            if (context.extra) {
                scope.setExtras(context.extra);
            }
            hub.captureException(reason, { originalException: promise });
        });
        this._handleRejection(reason);
    }
    // tslint:disable: no-console
    _handleRejection(reason) {
        // https://github.com/nodejs/node/blob/7cf6f9e964aa00772965391c23acda6d71972a9a/lib/internal/process/promises.js#L234-L240
        const rejectionWarning = 'This error originated either by ' +
            'throwing inside of an async function without a catch block, ' +
            'or by rejecting a promise which was not handled with .catch().' +
            ' The promise rejected with the reason:';
        /* -disable no-console */
        if (this._options.mode === 'warn') {
            utils_1.consoleSandbox(() => {
                console.warn(rejectionWarning);
                console.error(reason && reason.stack ? reason.stack : reason);
            });
        }
        else if (this._options.mode === 'strict') {
            utils_1.consoleSandbox(() => {
                console.warn(rejectionWarning);
            });
            handlers_1.logAndExitProcess(reason);
        }
    }
}
exports.OnUnhandledRejection = OnUnhandledRejection;
/**
 * @inheritDoc
 */
OnUnhandledRejection.id = 'OnUnhandledRejection';
//# sourceMappingURL=onunhandledrejection.js.map
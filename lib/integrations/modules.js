Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = require("fs");
const path_1 = require("path");
let moduleCache;
/** Extract information about paths */
function getPaths() {
    try {
        return require.cache ? Object.keys(require.cache) : [];
    }
    catch (e) {
        return [];
    }
}
/** Extract information about package.json modules */
function collectModules() {
    const mainPaths = (require.main && require.main.paths) || [];
    const paths = getPaths();
    const infos = {};
    const seen = {};
    paths.forEach(path => {
        let dir = path;
        /** Traverse directories upward in the search of package.json file */
        const updir = () => {
            const orig = dir;
            dir = path_1.dirname(orig);
            if (!dir || orig === dir || seen[orig]) {
                return undefined;
            }
            if (mainPaths.indexOf(dir) < 0) {
                return updir();
            }
            const pkgfile = path_1.join(orig, 'package.json');
            seen[orig] = true;
            if (!fs_1.existsSync(pkgfile)) {
                return updir();
            }
            try {
                const info = JSON.parse(fs_1.readFileSync(pkgfile, 'utf8'));
                infos[info.name] = info.version;
            }
            catch (_oO) {
                // no-empty
            }
        };
        updir();
    });
    return infos;
}
/** Add node modules / packages to the event */
class Modules {
    constructor() {
        /**
         * @inheritDoc
         */
        this.name = Modules.id;
    }
    /**
     * @inheritDoc
     */
    setupOnce(addGlobalEventProcessor, getCurrentHub) {
        addGlobalEventProcessor(event => {
            if (!getCurrentHub().getIntegration(Modules)) {
                return event;
            }
            return Object.assign(Object.assign({}, event), { modules: this._getModules() });
        });
    }
    /** Fetches the list of modules and the versions loaded by the entry file for your node.js app. */
    _getModules() {
        if (!moduleCache) {
            moduleCache = collectModules();
        }
        return moduleCache;
    }
}
exports.Modules = Modules;
/**
 * @inheritDoc
 */
Modules.id = 'Modules';
//# sourceMappingURL=modules.js.map
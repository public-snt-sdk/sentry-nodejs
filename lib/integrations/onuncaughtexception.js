Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("../core");
const types_1 = require("../types");
const utils_1 = require("../utils");
const handlers_1 = require("../handlers");
/** Global Promise Rejection handler */
class OnUncaughtException {
    /**
     * @inheritDoc
     */
    constructor(_options = {}) {
        this._options = _options;
        /**
         * @inheritDoc
         */
        this.name = OnUncaughtException.id;
        /**
         * @inheritDoc
         */
        this.handler = this._makeErrorHandler();
    }
    /**
     * @inheritDoc
     */
    setupOnce() {
        global.process.on('uncaughtException', this.handler.bind(this));
    }
    /**
     * @hidden
     */
    _makeErrorHandler() {
        const timeout = 2000;
        let caughtFirstError = false;
        let caughtSecondError = false;
        let calledFatalError = false;
        let firstError;
        return (error) => {
            let onFatalError = handlers_1.logAndExitProcess;
            const client = core_1.getCurrentHub().getClient();
            if (this._options.onFatalError) {
                onFatalError = this._options.onFatalError;
            }
            else if (client && client.getOptions().onFatalError) {
                onFatalError = client.getOptions().onFatalError;
            }
            if (!caughtFirstError) {
                const hub = core_1.getCurrentHub();
                // this is the first uncaught error and the ultimate reason for shutting down
                // we want to do absolutely everything possible to ensure it gets captured
                // also we want to make sure we don't go recursion crazy if more errors happen after this one
                firstError = error;
                caughtFirstError = true;
                if (hub.getIntegration(OnUncaughtException)) {
                    hub.withScope((scope) => {
                        scope.setLevel(types_1.Severity.Fatal);
                        hub.captureException(error, { originalException: error });
                        if (!calledFatalError) {
                            calledFatalError = true;
                            onFatalError(error);
                        }
                    });
                }
                else {
                    if (!calledFatalError) {
                        calledFatalError = true;
                        onFatalError(error);
                    }
                }
            }
            else if (calledFatalError) {
                // we hit an error *after* calling onFatalError - pretty boned at this point, just shut it down
                utils_1.logger.warn('uncaught exception after calling fatal error shutdown callback - this is bad! forcing shutdown');
                handlers_1.logAndExitProcess(error);
            }
            else if (!caughtSecondError) {
                caughtSecondError = true;
                setTimeout(() => {
                    if (!calledFatalError) {
                        // it was probably case 1, let's treat err as the sendErr and call onFatalError
                        calledFatalError = true;
                        onFatalError(firstError, error);
                    }
                    else {
                        // it was probably case 2, our first error finished capturing while we waited, cool, do nothing
                    }
                }, timeout);
            }
        };
    }
}
exports.OnUncaughtException = OnUncaughtException;
/**
 * @inheritDoc
 */
OnUncaughtException.id = 'OnUncaughtException';
//# sourceMappingURL=onuncaughtexception.js.map
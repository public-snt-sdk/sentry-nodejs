Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("../core");
const utils_1 = require("../utils");
const parsers_1 = require("../parsers");
const DEFAULT_KEY = 'cause';
const DEFAULT_LIMIT = 5;
/** Adds SDK info to an event. */
class LinkedErrors {
    /**
     * @inheritDoc
     */
    constructor(options = {}) {
        /**
         * @inheritDoc
         */
        this.name = LinkedErrors.id;
        this._key = options.key || DEFAULT_KEY;
        this._limit = options.limit || DEFAULT_LIMIT;
    }
    /**
     * @inheritDoc
     */
    setupOnce() {
        core_1.addGlobalEventProcessor((event, hint) => {
            const self = core_1.getCurrentHub().getIntegration(LinkedErrors);
            if (self) {
                const handler = self._handler && self._handler.bind(self);
                return typeof handler === 'function' ? handler(event, hint) : event;
            }
            return event;
        });
    }
    /**
     * @inheritDoc
     */
    _handler(event, hint) {
        if (!event.exception || !event.exception.values || !hint || !utils_1.isInstanceOf(hint.originalException, Error)) {
            return utils_1.SyncPromise.resolve(event);
        }
        return new utils_1.SyncPromise(resolve => {
            this._walkErrorTree(hint.originalException, this._key)
                .then((linkedErrors) => {
                if (event && event.exception && event.exception.values) {
                    event.exception.values = [...linkedErrors, ...event.exception.values];
                }
                resolve(event);
            })
                .then(null, () => {
                resolve(event);
            });
        });
    }
    /**
     * @inheritDoc
     */
    _walkErrorTree(error, key, stack = []) {
        if (!utils_1.isInstanceOf(error[key], Error) || stack.length + 1 >= this._limit) {
            return utils_1.SyncPromise.resolve(stack);
        }
        return new utils_1.SyncPromise((resolve, reject) => {
            parsers_1.getExceptionFromError(error[key])
                .then((exception) => {
                this._walkErrorTree(error[key], key, [exception, ...stack])
                    .then(resolve)
                    .then(null, () => {
                    reject();
                });
            })
                .then(null, () => {
                reject();
            });
        });
    }
}
exports.LinkedErrors = LinkedErrors;
/**
 * @inheritDoc
 */
LinkedErrors.id = 'LinkedErrors';
//# sourceMappingURL=linkederrors.js.map
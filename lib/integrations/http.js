Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("../core");
const utils_1 = require("../utils");
const http_1 = require("./utils/http");
const NODE_VERSION = utils_1.parseSemver(process.versions.node);
/** http module integration */
class Http {
    /**
     * @inheritDoc
     */
    constructor(options = {}) {
        /**
         * @inheritDoc
         */
        this.name = Http.id;
        this._breadcrumbs = typeof options.breadcrumbs === 'undefined' ? true : options.breadcrumbs;
        this._tracing = typeof options.tracing === 'undefined' ? false : options.tracing;
    }
    /**
     * @inheritDoc
     */
    setupOnce() {
        // No need to instrument if we don't want to track anything
        if (!this._breadcrumbs && !this._tracing) {
            return;
        }
        const wrappedHandlerMaker = _createWrappedRequestMethodFactory(this._breadcrumbs, this._tracing);
        const httpModule = require('http');
        utils_1.fill(httpModule, 'get', wrappedHandlerMaker);
        utils_1.fill(httpModule, 'request', wrappedHandlerMaker);
        // NOTE: Prior to Node 9, `https` used internals of `http` module, thus we don't patch it.
        // If we do, we'd get double breadcrumbs and double spans for `https` calls.
        // It has been changed in Node 9, so for all versions equal and above, we patch `https` separately.
        if (NODE_VERSION.major && NODE_VERSION.major > 8) {
            const httpsModule = require('https');
            utils_1.fill(httpsModule, 'get', wrappedHandlerMaker);
            utils_1.fill(httpsModule, 'request', wrappedHandlerMaker);
        }
    }
}
exports.Http = Http;
/**
 * @inheritDoc
 */
Http.id = 'Http';
/**
 * Function which creates a function which creates wrapped versions of internal `request` and `get` calls within `http`
 * and `https` modules. (NB: Not a typo - this is a creator^2!)
 *
 * @param breadcrumbsEnabled Whether or not to record outgoing requests as breadcrumbs
 * @param tracingEnabled Whether or not to record outgoing requests as tracing spans
 *
 * @returns A function which accepts the exiting handler and returns a wrapped handler
 */
function _createWrappedRequestMethodFactory(breadcrumbsEnabled, tracingEnabled) {
    return function wrappedRequestMethodFactory(originalRequestMethod) {
        return function wrappedMethod(...args) {
            // tslint:disable-next-line: no-this-assignment
            const httpModule = this;
            const requestArgs = http_1.normalizeRequestArgs(args);
            const requestOptions = requestArgs[0];
            const requestUrl = http_1.extractUrl(requestOptions);
            // we don't want to record requests to Sentry as either breadcrumbs or spans, so just use the original method
            if (http_1.isSentryRequest(requestUrl)) {
                return originalRequestMethod.apply(httpModule, requestArgs);
            }
            let span;
            let parentSpan;
            const scope = core_1.getCurrentHub().getScope();
            if (scope && tracingEnabled) {
                parentSpan = scope.getSpan();
                if (parentSpan) {
                    span = parentSpan.startChild({
                        description: `${requestOptions.method || 'GET'} ${requestUrl}`,
                        op: 'request',
                    });
                    const sentryTraceHeader = span.toTraceparent();
                    utils_1.logger.log(`[Tracing] Adding sentry-trace header to outgoing request: ${sentryTraceHeader}`);
                    requestOptions.headers = Object.assign(Object.assign({}, requestOptions.headers), { 'sentry-trace': sentryTraceHeader });
                }
            }
            return originalRequestMethod
                .apply(httpModule, requestArgs)
                .once('response', function (res) {
                // tslint:disable-next-line: no-this-assignment
                const req = this;
                if (breadcrumbsEnabled) {
                    addRequestBreadcrumb('response', requestUrl, req, res);
                }
                if (tracingEnabled && span) {
                    if (res.statusCode) {
                        span.setHttpStatus(res.statusCode);
                    }
                    span.description = http_1.cleanSpanDescription(span.description, requestOptions, req);
                    span.finish();
                }
            })
                .once('error', function () {
                // tslint:disable-next-line: no-this-assignment
                const req = this;
                if (breadcrumbsEnabled) {
                    addRequestBreadcrumb('error', requestUrl, req);
                }
                if (tracingEnabled && span) {
                    span.setHttpStatus(500);
                    span.description = http_1.cleanSpanDescription(span.description, requestOptions, req);
                    span.finish();
                }
            });
        };
    };
}
/**
 * Captures Breadcrumb based on provided request/response pair
 */
function addRequestBreadcrumb(event, url, req, res) {
    if (!core_1.getCurrentHub().getIntegration(Http)) {
        return;
    }
    core_1.getCurrentHub().addBreadcrumb({
        category: 'http',
        data: {
            method: req.method,
            status_code: res && res.statusCode,
            url,
        },
        type: 'http',
    }, {
        event,
        request: req,
        response: res,
    });
}
//# sourceMappingURL=http.js.map
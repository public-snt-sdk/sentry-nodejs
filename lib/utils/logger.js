Object.defineProperty(exports, "__esModule", { value: true });
/* -disable @typescript-/no-explicit-any */
const misc_1 = require("./misc");
// TODO: Implement different loggers for different environments
const global = misc_1.getGlobalObject();
/** Prefix for logging strings */
const PREFIX = 'Sentry Logger ';
/** JSDoc */
class Logger {
    /** JSDoc */
    constructor() {
        this._enabled = false;
    }
    /** JSDoc */
    disable() {
        this._enabled = false;
    }
    /** JSDoc */
    enable() {
        this._enabled = true;
    }
    /** JSDoc */
    log(...args) {
        if (!this._enabled) {
            return;
        }
        misc_1.consoleSandbox(() => {
            global.console.log(`${PREFIX}[Log]: ${args.join(' ')}`);
        });
    }
    /** JSDoc */
    warn(...args) {
        if (!this._enabled) {
            return;
        }
        misc_1.consoleSandbox(() => {
            global.console.warn(`${PREFIX}[Warn]: ${args.join(' ')}`);
        });
    }
    /** JSDoc */
    error(...args) {
        if (!this._enabled) {
            return;
        }
        misc_1.consoleSandbox(() => {
            global.console.error(`${PREFIX}[Error]: ${args.join(' ')}`);
        });
    }
}
// Ensure we only have a single logger instance, even if multiple versions of @sentry/utils are being used
global.__SENTRY__ = global.__SENTRY__ || {};
const logger = global.__SENTRY__.logger || (global.__SENTRY__.logger = new Logger());
exports.logger = logger;
//# sourceMappingURL=logger.js.map
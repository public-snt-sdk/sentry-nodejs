Object.defineProperty(exports, "__esModule", { value: true });
const polyfill_1 = require("./polyfill");
/** An error emitted by Sentry SDKs and related utilities. */
class SentryError extends Error {
    constructor(message) {
        super(message);
        this.message = message;
        this.name = new.target.prototype.constructor.name;
        polyfill_1.setPrototypeOf(this, new.target.prototype);
    }
}
exports.SentryError = SentryError;
//# sourceMappingURL=error.js.map
Object.defineProperty(exports, "__esModule", { value: true });
const error_1 = require("./error");
/** Regular expression used to parse a Dsn. */
const DSN_REGEX = /^(?:(\w+):)\/\/(?:(\w+)(?:-(\w+))?@)([\w.-]+)(?::(\d+))?\/(.+)/;
/** Error message */
const ERROR_MESSAGE = 'Invalid Dsn';
/** The Sentry Dsn, identifying a Sentry instance and project. */
class Dsn {
    /** Creates a new Dsn component */
    constructor(from) {
        if (typeof from === 'string') {
            this._fromString(from);
        }
        else {
            this._fromComponents(from);
        }
        this._validate();
    }
    /**
     * Renders the string representation of this Dsn.
     *
     * By default, this will render the public representation without the password
     * component. To get the deprecated private representation, set `withPassword`
     * to true.
     *
     * @param withPassword When set to true, the password will be included.
     */
    toString(withPassword = false) {
        // tslint:disable-next-line: no-this-assignment
        const { host, path, pass, port, projectId, protocol, publicKey } = this;
        return (`${protocol}://${publicKey}${withPassword && pass ? `-${pass}` : ''}` +
            `@${host}${port ? `:${port}` : ''}/${path ? `${path}/` : path}${projectId}`);
    }
    /** Parses a string into this Dsn. */
    _fromString(str) {
        const match = DSN_REGEX.exec(str);
        if (!match) {
            throw new error_1.SentryError(ERROR_MESSAGE);
        }
        const [protocol, publicKey, pass = '', host, port = '', lastPath] = match.slice(1);
        let path = '';
        let projectId = lastPath;
        const split = projectId.split('/');
        if (split.length > 1) {
            path = split.slice(0, -1).join('/');
            projectId = split.pop();
        }
        if (projectId) {
            const projectMatch = projectId.match(/^[\d\w+]{0,24}$/);
            if (projectMatch) {
                projectId = projectMatch[0];
            }
        }
        this._fromComponents({ host, pass, path, projectId, port, protocol: protocol, publicKey });
    }
    /** Maps Dsn components into this instance. */
    _fromComponents(components) {
        // TODO this is for backwards compatibility, and can be removed in a future version
        if ('user' in components && !('publicKey' in components)) {
            components.publicKey = components.user;
        }
        this.user = components.publicKey || '';
        this.protocol = components.protocol;
        this.publicKey = components.publicKey || '';
        this.pass = components.pass || '';
        this.host = components.host;
        this.port = components.port || '';
        this.path = components.path || '';
        this.projectId = components.projectId;
    }
    /** Validates this Dsn and throws on error. */
    _validate() {
        ['protocol', 'publicKey', 'host', 'projectId'].forEach(component => {
            if (!this[component]) {
                throw new error_1.SentryError(`${ERROR_MESSAGE}: ${component} missing`);
            }
        });
        if (!this.projectId.match(/^[\d\w+]{0,24}$/)) {
            throw new error_1.SentryError(`${ERROR_MESSAGE}: Invalid projectId ${this.projectId}`);
        }
        if (this.protocol !== 'http' && this.protocol !== 'https') {
            throw new error_1.SentryError(`${ERROR_MESSAGE}: Invalid protocol ${this.protocol}`);
        }
        if (this.port && isNaN(parseInt(this.port, 10))) {
            throw new error_1.SentryError(`${ERROR_MESSAGE}: Invalid port ${this.port}`);
        }
    }
}
exports.Dsn = Dsn;
//# sourceMappingURL=dsn.js.map
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("./core");
const utils_1 = require("./utils");
const domain = require("domain");
const os = require("os");
const url = require("url");
const sdk_1 = require("./sdk");
const DEFAULT_SHUTDOWN_TIMEOUT = 2000;
/**
 * Extracts complete generalized path from the request object and uses it to construct transaction name.
 *
 * eg. GET /mountpoint/user/:id
 *
 * @param req The ExpressRequest object
 * @param options What to include in the transaction name (method, path, or both)
 *
 * @returns The fully constructed transaction name
 */
function extractExpressTransactionName(req, options = {}) {
    var _a;
    const method = (_a = req.method) === null || _a === void 0 ? void 0 : _a.toUpperCase();
    let path = '';
    if (req.route) {
        // if the mountpoint is `/`, req.baseUrl is '' (not undefined), so it's safe to include it here
        // see https://github.com/expressjs/express/blob/508936853a6e311099c9985d4c11a4b1b8f6af07/test/req.baseUrl.js#L7
        path = `${req.baseUrl}${req.route.path}`;
    }
    else if (req.originalUrl || req.url) {
        path = utils_1.stripUrlQueryAndFragment(req.originalUrl || req.url || '');
    }
    let info = '';
    if (options.method && method) {
        info += method;
    }
    if (options.method && options.path) {
        info += ` `;
    }
    if (options.path && path) {
        info += path;
    }
    return info;
}
/** JSDoc */
function extractTransaction(req, type) {
    var _a;
    switch (type) {
        case 'path': {
            return extractExpressTransactionName(req, { path: true });
        }
        case 'handler': {
            return ((_a = req.route) === null || _a === void 0 ? void 0 : _a.stack[0].name) || '<anonymous>';
        }
        case 'methodPath':
        default: {
            return extractExpressTransactionName(req, { path: true, method: true });
        }
    }
}
/** Default user keys that'll be used to extract data from the request */
const DEFAULT_USER_KEYS = ['id', 'username', 'email'];
/** JSDoc */
function extractUserData(user, keys) {
    const extractedUser = {};
    const attributes = Array.isArray(keys) ? keys : DEFAULT_USER_KEYS;
    attributes.forEach(key => {
        if (user && key in user) {
            extractedUser[key] = user[key];
        }
    });
    return extractedUser;
}
/** Default request keys that'll be used to extract data from the request */
const DEFAULT_REQUEST_KEYS = ['data', 'headers', 'method', 'query_string', 'url'];
/**
 * Normalizes data from the request object, accounting for framework differences.
 *
 * @param req The request object from which to extract data
 * @param keys An optional array of keys to include in the normalized data. Defaults to DEFAULT_REQUEST_KEYS if not
 * provided.
 * @returns An object containing normalized request data
 */
function extractRequestData(req, keys = DEFAULT_REQUEST_KEYS) {
    const requestData = {};
    // headers:
    //   node, express: req.headers
    //   koa: req.header
    const headers = (req.headers || req.header || {});
    // method:
    //   node, express, koa: req.method
    const method = req.method;
    // host:
    //   express: req.hostname in > 4 and req.host in < 4
    //   koa: req.host
    //   node: req.headers.host
    const host = req.hostname || req.host || headers.host || '<no host>';
    // protocol:
    //   node: <n/a>
    //   express, koa: req.protocol
    const protocol = req.protocol === 'https' || req.secure || (req.socket || {}).encrypted
        ? 'https'
        : 'http';
    // url (including path and query string):
    //   node, express: req.originalUrl
    //   koa: req.url
    const originalUrl = (req.originalUrl || req.url || '');
    // absolute url
    const absoluteUrl = `${protocol}://${host}${originalUrl}`;
    keys.forEach(key => {
        switch (key) {
            case 'headers':
                requestData.headers = headers;
                break;
            case 'method':
                requestData.method = method;
                break;
            case 'url':
                requestData.url = absoluteUrl;
                break;
            case 'query_string':
                requestData.query_string = url.parse(originalUrl || '', false).query;
                break;
            case 'data':
                if (method === 'GET' || method === 'HEAD') {
                    break;
                }
                // body data:
                //   node, express, koa: req.body
                if (req.body !== undefined) {
                    requestData.data = utils_1.isString(req.body) ? req.body : JSON.stringify(utils_1.normalize(req.body));
                }
                break;
            default:
                if ({}.hasOwnProperty.call(req, key)) {
                    requestData[key] = req[key];
                }
        }
    });
    return requestData;
}
exports.extractRequestData = extractRequestData;
function parseRequest(event, req, options) {
    options = Object.assign({ ip: false, request: true, serverName: true, transaction: true, user: true, version: true }, options);
    if (options.version) {
        event.contexts = Object.assign(Object.assign({}, event.contexts), { runtime: {
                name: 'node',
                version: global.process.version,
            } });
    }
    if (options.request) {
        // if the option value is `true`, use the default set of keys by not passing anything to `extractRequestData()`
        const extractedRequestData = Array.isArray(options.request)
            ? extractRequestData(req, options.request)
            : extractRequestData(req);
        event.request = Object.assign(Object.assign({}, event.request), extractedRequestData);
    }
    if (options.serverName && !event.server_name) {
        event.server_name = global.process.env.SENTRY_NAME || os.hostname();
    }
    if (options.user) {
        const extractedUser = req.user && utils_1.isPlainObject(req.user) ? extractUserData(req.user, options.user) : {};
        if (Object.keys(extractedUser)) {
            event.user = Object.assign(Object.assign({}, event.user), extractedUser);
        }
    }
    // client ip:
    //   node: req.connection.remoteAddress
    //   express, koa: req.ip
    if (options.ip) {
        const ip = req.ip || (req.connection && req.connection.remoteAddress);
        if (ip) {
            event.user = Object.assign(Object.assign({}, event.user), { ip_address: ip });
        }
    }
    if (options.transaction && !event.transaction) {
        event.transaction = extractTransaction(req, options.transaction);
    }
    return event;
}
exports.parseRequest = parseRequest;
/**
 * Express compatible request handler.
 * @see Exposed as `Handlers.requestHandler`
 */
function requestHandler(options) {
    return function sentryRequestMiddleware(req, res, next) {
        if (options && options.flushTimeout && options.flushTimeout > 0) {
            const _end = res.end;
            res.end = function (chunk, encoding, cb) {
                sdk_1.flush(options.flushTimeout)
                    .then(() => {
                    _end.call(this, chunk, encoding, cb);
                })
                    .then(null, e => {
                    utils_1.logger.error(e);
                });
            };
        }
        const local = domain.create();
        local.add(req);
        local.add(res);
        local.on('error', next);
        local.run(() => {
            core_1.getCurrentHub().configureScope(scope => scope.addEventProcessor((event) => parseRequest(event, req, options)));
            next();
        });
    };
}
exports.requestHandler = requestHandler;
/** JSDoc */
function getStatusCodeFromResponse(error) {
    const statusCode = error.status || error.statusCode || error.status_code || (error.output && error.output.statusCode);
    return statusCode ? parseInt(statusCode, 10) : 500;
}
/** Returns true if response code is internal server error */
function defaultShouldHandleError(error) {
    const status = getStatusCodeFromResponse(error);
    return status >= 500;
}
/**
 * Express compatible error handler.
 * @see Exposed as `Handlers.errorHandler`
 */
function errorHandler(options) {
    return function sentryErrorMiddleware(error, _req, res, next) {
        const shouldHandleError = (options && options.shouldHandleError) || defaultShouldHandleError;
        if (shouldHandleError(error)) {
            core_1.withScope(_scope => {
                const transaction = res.__sentry_transaction;
                if (transaction && _scope.getSpan() === undefined) {
                    _scope.setSpan(transaction);
                }
                const eventId = core_1.captureException(error);
                res.sentry = eventId;
                next(error);
            });
            return;
        }
        next(error);
    };
}
exports.errorHandler = errorHandler;
/**
 * @hidden
 */
function logAndExitProcess(_error) {
    const client = core_1.getCurrentHub().getClient();
    if (client === undefined) {
        utils_1.logger.warn('No NodeClient was defined, we are exiting the process now.');
        global.process.exit(1);
        return;
    }
    const options = client.getOptions();
    const timeout = (options && options.shutdownTimeout && options.shutdownTimeout > 0 && options.shutdownTimeout) ||
        DEFAULT_SHUTDOWN_TIMEOUT;
    utils_1.forget(client.close(timeout).then((result) => {
        if (!result) {
            utils_1.logger.warn('We reached the timeout for emptying the request buffer, still exiting now!');
        }
        global.process.exit(1);
    }));
}
exports.logAndExitProcess = logAndExitProcess;
//# sourceMappingURL=handlers.js.map
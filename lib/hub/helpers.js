Object.defineProperty(exports, "__esModule", { value: true });
const errorTypesNeedCreateInstance = ['string', 'object'];
exports.getErrorMessage = (originalError) => {
    if (typeof originalError === 'string') {
        return {
            name: 'ERROR',
            message: originalError,
        };
    }
    const name = originalError.name || originalError.errorName || originalError.error || 'ERROR';
    const error = {
        name,
        message: '',
    };
    for (const key in originalError) {
        if (!originalError[key]) {
            continue;
        }
        if (typeof originalError[key] === 'string') {
            error.message += ` ${originalError[key]}`;
            continue;
        }
        error.message += ` ${JSON.stringify(originalError[key])}`;
    }
    return error;
};
exports.handleError = (exception) => {
    if (exception instanceof Error) {
        return exception;
    }
    if (errorTypesNeedCreateInstance.includes(typeof (exception))) {
        const error = new Error();
        const payload = exports.getErrorMessage(exception);
        error.message = payload.message;
        error.name = payload.name;
        return error;
    }
    return exception;
};
//# sourceMappingURL=helpers.js.map
Object.defineProperty(exports, "__esModule", { value: true });
const types_1 = require("../types");
const utils_1 = require("../utils");
/**
 * @inheritdoc
 */
class Session {
    constructor(context) {
        this.errors = 0;
        this.sid = utils_1.uuid4();
        this.timestamp = Date.now();
        this.started = Date.now();
        this.duration = 0;
        this.status = types_1.SessionStatus.Ok;
        this.init = true;
        if (context) {
            this.update(context);
        }
    }
    // tslint:disable-next-line: cyclomatic-complexity
    update(context = {}) {
        if (context.user) {
            if (context.user.ip_address) {
                this.ipAddress = context.user.ip_address;
            }
            if (!context.did) {
                this.did = context.user.id || context.user.email || context.user.username;
            }
        }
        this.timestamp = context.timestamp || Date.now();
        if (context.sid) {
            // Good enough uuid validation. — Kamil
            this.sid = context.sid.length === 32 ? context.sid : utils_1.uuid4();
        }
        if (context.init !== undefined) {
            this.init = context.init;
        }
        if (context.did) {
            this.did = `${context.did}`;
        }
        if (typeof context.started === 'number') {
            this.started = context.started;
        }
        // tslint:disable-next-line: prefer-conditional-expression
        if (typeof context.duration === 'number') {
            this.duration = context.duration;
        }
        else {
            this.duration = this.timestamp - this.started;
        }
        if (context.release) {
            this.release = context.release;
        }
        if (context.environment) {
            this.environment = context.environment;
        }
        if (context.ipAddress) {
            this.ipAddress = context.ipAddress;
        }
        if (context.userAgent) {
            this.userAgent = context.userAgent;
        }
        if (typeof context.errors === 'number') {
            this.errors = context.errors;
        }
        if (context.status) {
            this.status = context.status;
        }
    }
    /** JSDoc */
    close(status) {
        if (status) {
            this.update({ status });
        }
        else if (this.status === types_1.SessionStatus.Ok) {
            this.update({ status: types_1.SessionStatus.Exited });
        }
        else {
            this.update();
        }
    }
    /** JSDoc */
    toJSON() {
        return utils_1.dropUndefinedKeys({
            sid: `${this.sid}`,
            init: this.init,
            started: new Date(this.started).toISOString(),
            timestamp: new Date(this.timestamp).toISOString(),
            status: this.status,
            errors: this.errors,
            did: typeof this.did === 'number' || typeof this.did === 'string' ? `${this.did}` : undefined,
            duration: this.duration,
            attrs: utils_1.dropUndefinedKeys({
                release: this.release,
                environment: this.environment,
                ip_address: this.ipAddress,
                user_agent: this.userAgent,
            }),
        });
    }
}
exports.Session = Session;
//# sourceMappingURL=session.js.map
Object.defineProperty(exports, "__esModule", { value: true });
const utils_1 = require("../utils");
/**
 * Holds additional event information. {@link Scope.applyToEvent} will be
 * called by the client before an event will be sent.
 */
class Scope {
    constructor() {
        /** Flag if notifiying is happening. */
        this._notifyingListeners = false;
        /** Callback for client to receive scope changes. */
        this._scopeListeners = [];
        /** Callback list that will be called after {@link applyToEvent}. */
        this._eventProcessors = [];
        /** Array of breadcrumbs. */
        this._breadcrumbs = [];
        /** User */
        this._user = {};
        /** Tags */
        this._tags = {};
        /** Extra */
        this._extra = {};
        /** Contexts */
        this._contexts = {};
    }
    /**
     * Inherit values from the parent scope.
     * @param scope to clone.
     */
    static clone(scope) {
        const newScope = new Scope();
        if (scope) {
            newScope._breadcrumbs = [...scope._breadcrumbs];
            newScope._tags = Object.assign({}, scope._tags);
            newScope._extra = Object.assign({}, scope._extra);
            newScope._contexts = Object.assign({}, scope._contexts);
            newScope._user = scope._user;
            newScope._level = scope._level;
            newScope._span = scope._span;
            newScope._session = scope._session;
            newScope._transactionName = scope._transactionName;
            newScope._fingerprint = scope._fingerprint;
            newScope._eventProcessors = [...scope._eventProcessors];
        }
        return newScope;
    }
    /**
     * Add internal on change listener. Used for sub SDKs that need to store the scope.
     * @hidden
     */
    addScopeListener(callback) {
        this._scopeListeners.push(callback);
    }
    /**
     * @inheritDoc
     */
    addEventProcessor(callback) {
        this._eventProcessors.push(callback);
        return this;
    }
    /**
     * @inheritDoc
     */
    setUser(user) {
        this._user = user || {};
        if (this._session) {
            this._session.update({ user });
        }
        this._notifyScopeListeners();
        return this;
    }
    /**
     * @inheritDoc
     */
    getUser() {
        return this._user;
    }
    /**
     * @inheritDoc
     */
    setTags(tags) {
        this._tags = Object.assign(Object.assign({}, this._tags), tags);
        this._notifyScopeListeners();
        return this;
    }
    /**
     * @inheritDoc
     */
    setTag(key, value) {
        this._tags = Object.assign(Object.assign({}, this._tags), { [key]: value });
        this._notifyScopeListeners();
        return this;
    }
    /**
     * @inheritDoc
     */
    setExtras(extras) {
        this._extra = Object.assign(Object.assign({}, this._extra), extras);
        this._notifyScopeListeners();
        return this;
    }
    /**
     * @inheritDoc
     */
    setExtra(key, extra) {
        this._extra = Object.assign(Object.assign({}, this._extra), { [key]: extra });
        this._notifyScopeListeners();
        return this;
    }
    /**
     * @inheritDoc
     */
    setFingerprint(fingerprint) {
        this._fingerprint = fingerprint;
        this._notifyScopeListeners();
        return this;
    }
    /**
     * @inheritDoc
     */
    setLevel(level) {
        this._level = level;
        this._notifyScopeListeners();
        return this;
    }
    /**
     * @inheritDoc
     */
    setTransactionName(name) {
        this._transactionName = name;
        this._notifyScopeListeners();
        return this;
    }
    /**
     * Can be removed in major version.
     * @deprecated in favor of {@link this.setTransactionName}
     */
    setTransaction(name) {
        return this.setTransactionName(name);
    }
    /**
     * @inheritDoc
     */
    setContext(key, context) {
        if (context === null) {
            delete this._contexts[key];
        }
        else {
            this._contexts = Object.assign(Object.assign({}, this._contexts), { [key]: context });
        }
        this._notifyScopeListeners();
        return this;
    }
    /**
     * @inheritDoc
     */
    setSpan(span) {
        this._span = span;
        this._notifyScopeListeners();
        return this;
    }
    /**
     * @inheritDoc
     */
    getSpan() {
        return this._span;
    }
    /**
     * @inheritDoc
     */
    getTransaction() {
        var _a, _b, _c, _d;
        // often, this span will be a transaction, but it's not guaranteed to be
        const span = this.getSpan();
        // try it the new way first
        if ((_a = span) === null || _a === void 0 ? void 0 : _a.transaction) {
            return (_b = span) === null || _b === void 0 ? void 0 : _b.transaction;
        }
        // fallback to the old way (known bug: this only finds transactions with sampled = true)
        if ((_d = (_c = span) === null || _c === void 0 ? void 0 : _c.spanRecorder) === null || _d === void 0 ? void 0 : _d.spans[0]) {
            return span.spanRecorder.spans[0];
        }
        // neither way found a transaction
        return undefined;
    }
    /**
     * @inheritDoc
     */
    setSession(session) {
        if (!session) {
            delete this._session;
        }
        else {
            this._session = session;
        }
        this._notifyScopeListeners();
        return this;
    }
    /**
     * @inheritDoc
     */
    getSession() {
        return this._session;
    }
    /**
     * @inheritDoc
     */
    update(captureContext) {
        if (!captureContext) {
            return this;
        }
        if (typeof captureContext === 'function') {
            const updatedScope = captureContext(this);
            return updatedScope instanceof Scope ? updatedScope : this;
        }
        if (captureContext instanceof Scope) {
            this._tags = Object.assign(Object.assign({}, this._tags), captureContext._tags);
            this._extra = Object.assign(Object.assign({}, this._extra), captureContext._extra);
            this._contexts = Object.assign(Object.assign({}, this._contexts), captureContext._contexts);
            if (captureContext._user && Object.keys(captureContext._user).length) {
                this._user = captureContext._user;
            }
            if (captureContext._level) {
                this._level = captureContext._level;
            }
            if (captureContext._fingerprint) {
                this._fingerprint = captureContext._fingerprint;
            }
        }
        else if (utils_1.isPlainObject(captureContext)) {
            captureContext = captureContext;
            this._tags = Object.assign(Object.assign({}, this._tags), captureContext.tags);
            this._extra = Object.assign(Object.assign({}, this._extra), captureContext.extra);
            this._contexts = Object.assign(Object.assign({}, this._contexts), captureContext.contexts);
            if (captureContext.user) {
                this._user = captureContext.user;
            }
            if (captureContext.level) {
                this._level = captureContext.level;
            }
            if (captureContext.fingerprint) {
                this._fingerprint = captureContext.fingerprint;
            }
        }
        return this;
    }
    /**
     * @inheritDoc
     */
    clear() {
        this._breadcrumbs = [];
        this._tags = {};
        this._extra = {};
        this._user = {};
        this._contexts = {};
        this._level = undefined;
        this._transactionName = undefined;
        this._fingerprint = undefined;
        this._span = undefined;
        this._session = undefined;
        this._notifyScopeListeners();
        return this;
    }
    /**
     * @inheritDoc
     */
    addBreadcrumb(breadcrumb, maxBreadcrumbs) {
        const mergedBreadcrumb = Object.assign({ timestamp: utils_1.dateTimestampInSeconds() }, breadcrumb);
        this._breadcrumbs =
            maxBreadcrumbs !== undefined && maxBreadcrumbs >= 0
                ? [...this._breadcrumbs, mergedBreadcrumb].slice(-maxBreadcrumbs)
                : [...this._breadcrumbs, mergedBreadcrumb];
        this._notifyScopeListeners();
        return this;
    }
    /**
     * @inheritDoc
     */
    clearBreadcrumbs() {
        this._breadcrumbs = [];
        this._notifyScopeListeners();
        return this;
    }
    /**
     * Applies the current context and fingerprint to the event.
     * Note that breadcrumbs will be added by the client.
     * Also if the event has already breadcrumbs on it, we do not merge them.
     * @param event Event
     * @param hint May contain additional informartion about the original exception.
     * @hidden
     */
    applyToEvent(event, hint) {
        var _a;
        if (this._extra && Object.keys(this._extra).length) {
            event.extra = Object.assign(Object.assign({}, this._extra), event.extra);
        }
        if (this._tags && Object.keys(this._tags).length) {
            event.tags = Object.assign(Object.assign({}, this._tags), event.tags);
        }
        if (this._user && Object.keys(this._user).length) {
            event.user = Object.assign(Object.assign({}, this._user), event.user);
        }
        if (this._contexts && Object.keys(this._contexts).length) {
            event.contexts = Object.assign(Object.assign({}, this._contexts), event.contexts);
        }
        if (this._level) {
            event.level = this._level;
        }
        if (this._transactionName) {
            event.transaction = this._transactionName;
        }
        // We want to set the trace context for normal events only if there isn't already
        // a trace context on the event. There is a product feature in place where we link
        // errors with transaction and it relys on that.
        if (this._span) {
            event.contexts = Object.assign({ trace: this._span.getTraceContext() }, event.contexts);
            const transactionName = (_a = this._span.transaction) === null || _a === void 0 ? void 0 : _a.name;
            if (transactionName) {
                event.tags = Object.assign({ transaction: transactionName }, event.tags);
            }
        }
        this._applyFingerprint(event);
        event.breadcrumbs = [...(event.breadcrumbs || []), ...this._breadcrumbs];
        event.breadcrumbs = event.breadcrumbs.length > 0 ? event.breadcrumbs : undefined;
        return this._notifyEventProcessors([...getGlobalEventProcessors(), ...this._eventProcessors], event, hint);
    }
    /**
     * This will be called after {@link applyToEvent} is finished.
     */
    _notifyEventProcessors(processors, event, hint, index = 0) {
        return new utils_1.SyncPromise((resolve, reject) => {
            const processor = processors[index];
            if (event === null || typeof processor !== 'function') {
                resolve(event);
            }
            else {
                const result = processor(Object.assign({}, event), hint);
                if (utils_1.isThenable(result)) {
                    result
                        .then(final => this._notifyEventProcessors(processors, final, hint, index + 1).then(resolve))
                        .then(null, reject);
                }
                else {
                    this._notifyEventProcessors(processors, result, hint, index + 1)
                        .then(resolve)
                        .then(null, reject);
                }
            }
        });
    }
    /**
     * This will be called on every set call.
     */
    _notifyScopeListeners() {
        // We need this check for this._notifyingListeners to be able to work on scope during updates
        // If this check is not here we'll produce endless recursion when something is done with the scope
        // during the callback.
        if (!this._notifyingListeners) {
            this._notifyingListeners = true;
            this._scopeListeners.forEach(callback => {
                callback(this);
            });
            this._notifyingListeners = false;
        }
    }
    /**
     * Applies fingerprint from the scope to the event if there's one,
     * uses message if there's one instead or get rid of empty fingerprint
     */
    _applyFingerprint(event) {
        // Make sure it's an array first and we actually have something in place
        event.fingerprint = event.fingerprint
            ? Array.isArray(event.fingerprint)
                ? event.fingerprint
                : [event.fingerprint]
            : [];
        // If we have something on the scope, then merge it with event
        if (this._fingerprint) {
            event.fingerprint = event.fingerprint.concat(this._fingerprint);
        }
        // If we have no data at all, remove empty array default
        if (event.fingerprint && !event.fingerprint.length) {
            delete event.fingerprint;
        }
    }
}
exports.Scope = Scope;
/**
 * Retruns the global event processors.
 */
function getGlobalEventProcessors() {
    const global = utils_1.getGlobalObject();
    global.__SENTRY__ = global.__SENTRY__ || {};
    global.__SENTRY__.globalEventProcessors = global.__SENTRY__.globalEventProcessors || [];
    return global.__SENTRY__.globalEventProcessors;
    /* -enable @typescript-/no-explicit-any, @typescript-/no-unsafe-member-access */
}
/**
 * Add a EventProcessor to be kept globally.
 * @param callback EventProcessor to add
 */
function addGlobalEventProcessor(callback) {
    getGlobalEventProcessors().push(callback);
}
exports.addGlobalEventProcessor = addGlobalEventProcessor;
//# sourceMappingURL=scope.js.map
Object.defineProperty(exports, "__esModule", { value: true });
const types_1 = require("../types");
const utils_1 = require("../utils");
const helpers_1 = require("./helpers");
const scope_1 = require("./scope");
const session_1 = require("./session");
/**
 * API compatibility version of this hub.
 *
 * WARNING: This number should only be increased when the global interface
 * changes and new methods are introduced.
 *
 * @hidden
 */
exports.API_VERSION = 3;
/**
 * Default maximum number of breadcrumbs added to an event. Can be overwritten
 * with {@link Options.maxBreadcrumbs}.
 */
const DEFAULT_BREADCRUMBS = 100;
/**
 * Absolute maximum number of breadcrumbs added to an event. The
 * `maxBreadcrumbs` option cannot be higher than this value.
 */
const MAX_BREADCRUMBS = 100;
/**
 * @inheritDoc
 */
class Hub {
    /**
     * Creates a new instance of the hub, will push one {@link Layer} into the
     * internal stack on creation.
     *
     * @param client bound to the hub.
     * @param scope bound to the hub.
     * @param version number, higher number means higher priority.
     */
    constructor(client, scope = new scope_1.Scope(), _version = exports.API_VERSION) {
        this._version = _version;
        /** Is a {@link Layer}[] containing the client and scope */
        this._stack = [{}];
        this.getStackTop().scope = scope;
        this.bindClient(client);
    }
    /**
     * @inheritDoc
     */
    isOlderThan(version) {
        return this._version < version;
    }
    /**
     * @inheritDoc
     */
    bindClient(client) {
        const top = this.getStackTop();
        top.client = client;
        if (client && client.setupIntegrations) {
            client.setupIntegrations();
        }
    }
    /**
     * @inheritDoc
     */
    pushScope() {
        // We want to clone the content of prev scope
        const scope = scope_1.Scope.clone(this.getScope());
        this.getStack().push({
            client: this.getClient(),
            scope,
        });
        return scope;
    }
    /**
     * @inheritDoc
     */
    popScope() {
        if (this.getStack().length <= 1)
            return false;
        return !!this.getStack().pop();
    }
    /**
     * @inheritDoc
     */
    withScope(callback) {
        const scope = this.pushScope();
        try {
            callback(scope);
        }
        finally {
            this.popScope();
        }
    }
    /**
     * @inheritDoc
     */
    getClient() {
        return this.getStackTop().client;
    }
    /** Returns the scope of the top stack. */
    getScope() {
        return this.getStackTop().scope;
    }
    /** Returns the scope stack for domains or the process. */
    getStack() {
        return this._stack;
    }
    /** Returns the topmost scope layer in the order domain > local > process. */
    getStackTop() {
        return this._stack[this._stack.length - 1];
    }
    /**
     * @inheritDoc
     */
    captureException(exception, hint) {
        const correctException = helpers_1.handleError(exception);
        const eventId = (this._lastEventId = utils_1.uuid4());
        let finalHint = hint;
        // If there's no explicit hint provided, mimick the same thing that would happen
        // in the minimal itself to create a consistent behavior.
        // We don't do this in the client, as it's the lowest level API, and doing this,
        // would prevent user from having full control over direct calls.
        if (!hint) {
            let syntheticException;
            try {
                throw new Error('Sentry syntheticException');
            }
            catch (exception) {
                syntheticException = exception;
            }
            finalHint = {
                originalException: correctException,
                syntheticException,
            };
        }
        this._invokeClient('captureException', correctException, Object.assign(Object.assign({}, finalHint), { event_id: eventId }));
        return eventId;
    }
    /**
     * @inheritDoc
     */
    captureMessage(message, level, hint) {
        const eventId = (this._lastEventId = utils_1.uuid4());
        let finalHint = hint;
        // If there's no explicit hint provided, mimick the same thing that would happen
        // in the minimal itself to create a consistent behavior.
        // We don't do this in the client, as it's the lowest level API, and doing this,
        // would prevent user from having full control over direct calls.
        if (!hint) {
            let syntheticException;
            try {
                throw new Error(message);
            }
            catch (exception) {
                syntheticException = exception;
            }
            finalHint = {
                originalException: message,
                syntheticException,
            };
        }
        this._invokeClient('captureMessage', message, level, Object.assign(Object.assign({}, finalHint), { event_id: eventId }));
        return eventId;
    }
    /**
     * @inheritDoc
     */
    captureEvent(event, hint) {
        const eventId = (this._lastEventId = utils_1.uuid4());
        this._invokeClient('captureEvent', event, Object.assign(Object.assign({}, hint), { event_id: eventId }));
        return eventId;
    }
    /**
     * @inheritDoc
     */
    lastEventId() {
        return this._lastEventId;
    }
    /**
     * @inheritDoc
     */
    addBreadcrumb(breadcrumb, hint) {
        const { scope, client } = this.getStackTop();
        if (!scope || !client)
            return;
        const { beforeBreadcrumb = null, maxBreadcrumbs = DEFAULT_BREADCRUMBS } = (client.getOptions && client.getOptions()) || {};
        if (maxBreadcrumbs <= 0)
            return;
        const timestamp = utils_1.dateTimestampInSeconds();
        const mergedBreadcrumb = Object.assign({ timestamp }, breadcrumb);
        const finalBreadcrumb = beforeBreadcrumb
            ? utils_1.consoleSandbox(() => beforeBreadcrumb(mergedBreadcrumb, hint))
            : mergedBreadcrumb;
        if (finalBreadcrumb === null)
            return;
        scope.addBreadcrumb(finalBreadcrumb, Math.min(maxBreadcrumbs, MAX_BREADCRUMBS));
    }
    /**
     * @inheritDoc
     */
    setUser(user) {
        const scope = this.getScope();
        if (scope)
            scope.setUser(user);
    }
    /**
     * @inheritDoc
     */
    setTags(tags) {
        const scope = this.getScope();
        if (scope)
            scope.setTags(tags);
    }
    /**
     * @inheritDoc
     */
    setExtras(extras) {
        const scope = this.getScope();
        if (scope)
            scope.setExtras(extras);
    }
    /**
     * @inheritDoc
     */
    setTag(key, value) {
        const scope = this.getScope();
        if (scope)
            scope.setTag(key, value);
    }
    /**
     * @inheritDoc
     */
    setExtra(key, extra) {
        const scope = this.getScope();
        if (scope)
            scope.setExtra(key, extra);
    }
    /**
     * @inheritDoc
     */
    setContext(name, context) {
        const scope = this.getScope();
        if (scope)
            scope.setContext(name, context);
    }
    /**
     * @inheritDoc
     */
    configureScope(callback) {
        const { scope, client } = this.getStackTop();
        if (scope && client) {
            callback(scope);
        }
    }
    /**
     * @inheritDoc
     */
    run(callback) {
        const oldHub = makeMain(this);
        try {
            callback(this);
        }
        finally {
            makeMain(oldHub);
        }
    }
    /**
     * @inheritDoc
     */
    getIntegration(integration) {
        const client = this.getClient();
        if (!client)
            return null;
        try {
            return client.getIntegration(integration);
        }
        catch (_oO) {
            utils_1.logger.warn(`Cannot retrieve integration ${integration.id} from the current Hub`);
            return null;
        }
    }
    /**
     * @inheritDoc
     */
    startSpan(context) {
        return this._callExtensionMethod('startSpan', context);
    }
    /**
     * @inheritDoc
     */
    startTransaction(context, customSamplingContext) {
        return this._callExtensionMethod('startTransaction', context, customSamplingContext);
    }
    /**
     * @inheritDoc
     */
    traceHeaders() {
        return this._callExtensionMethod('traceHeaders');
    }
    /**
     * @inheritDoc
     */
    captureSession(endSession = false) {
        // both send the update and pull the session from the scope
        if (endSession) {
            return this.endSession();
        }
        // only send the update
        this._sendSessionUpdate();
    }
    /**
     * @inheritDoc
     */
    endSession() {
        var _a, _b, _c, _d, _e;
        (_c = (_b = (_a = this.getStackTop()) === null || _a === void 0 ? void 0 : _a.scope) === null || _b === void 0 ? void 0 : _b.getSession()) === null || _c === void 0 ? void 0 : _c.close();
        this._sendSessionUpdate();
        // the session is over; take it off of the scope
        (_e = (_d = this.getStackTop()) === null || _d === void 0 ? void 0 : _d.scope) === null || _e === void 0 ? void 0 : _e.setSession();
    }
    /**
     * @inheritDoc
     */
    startSession(context) {
        const { scope, client } = this.getStackTop();
        const { release, environment } = (client && client.getOptions()) || {};
        const session = new session_1.Session(Object.assign(Object.assign({ release,
            environment }, (scope && { user: scope.getUser() })), context));
        if (scope) {
            // End existing session if there's one
            const currentSession = scope.getSession && scope.getSession();
            if (currentSession && currentSession.status === types_1.SessionStatus.Ok) {
                currentSession.update({ status: types_1.SessionStatus.Exited });
            }
            this.endSession();
            // Afterwards we set the new session on the scope
            scope.setSession(session);
        }
        return session;
    }
    /**
     * Sends the current Session on the scope
     */
    _sendSessionUpdate() {
        const { scope, client } = this.getStackTop();
        if (!scope)
            return;
        const session = scope.getSession && scope.getSession();
        if (session) {
            if (client && client.captureSession) {
                client.captureSession(session);
            }
        }
    }
    /**
     * Internal helper function to call a method on the top client if it exists.
     *
     * @param method The method to call on the client.
     * @param args Arguments to pass to the client function.
     */
    _invokeClient(method, ...args) {
        const { scope, client } = this.getStackTop();
        if (client && client[method]) {
            client[method](...args, scope);
        }
    }
    /**
     * Calls global extension method and binding current instance to the function call
     */
    // @ts-ignore Function lacks ending return statement and return type does not include 'undefined'. ts(2366)
    _callExtensionMethod(method, ...args) {
        const carrier = getMainCarrier();
        const sentry = carrier.__SENTRY__;
        if (sentry && sentry.extensions && typeof sentry.extensions[method] === 'function') {
            return sentry.extensions[method].apply(this, args);
        }
        utils_1.logger.warn(`Extension method ${method} couldn't be found, doing nothing.`);
    }
}
exports.Hub = Hub;
/** Returns the global shim registry. */
function getMainCarrier() {
    const carrier = utils_1.getGlobalObject();
    carrier.__SENTRY__ = carrier.__SENTRY__ || {
        extensions: {},
        hub: undefined,
    };
    return carrier;
}
exports.getMainCarrier = getMainCarrier;
/**
 * Replaces the current main hub with the passed one on the global object
 *
 * @returns The old replaced hub
 */
function makeMain(hub) {
    const registry = getMainCarrier();
    const oldHub = getHubFromCarrier(registry);
    setHubOnCarrier(registry, hub);
    return oldHub;
}
exports.makeMain = makeMain;
/**
 * Returns the default hub instance.
 *
 * If a hub is already registered in the global carrier but this module
 * contains a more recent version, it replaces the registered version.
 * Otherwise, the currently registered hub will be returned.
 */
function getCurrentHub() {
    // Get main carrier (global for every environment)
    const registry = getMainCarrier();
    // If there's no hub, or its an old API, assign a new one
    if (!hasHubOnCarrier(registry) || getHubFromCarrier(registry).isOlderThan(exports.API_VERSION)) {
        setHubOnCarrier(registry, new Hub());
    }
    // Prefer domains over global if they are there (applicable only to Node environment)
    if (utils_1.isNodeEnv()) {
        return getHubFromActiveDomain(registry);
    }
    // Return hub that lives on a global object
    return getHubFromCarrier(registry);
}
exports.getCurrentHub = getCurrentHub;
/**
 * Returns the active domain, if one exists
 * @deprecated No longer used; remove in v7
 * @returns The domain, or undefined if there is no active domain
 */
function getActiveDomain() {
    utils_1.logger.warn('Function `getActiveDomain` is deprecated and will be removed in a future version.');
    const sentry = getMainCarrier().__SENTRY__;
    return sentry && sentry.extensions && sentry.extensions.domain && sentry.extensions.domain.active;
}
exports.getActiveDomain = getActiveDomain;
/**
 * Try to read the hub from an active domain, and fallback to the registry if one doesn't exist
 * @returns discovered hub
 */
function getHubFromActiveDomain(registry) {
    var _a, _b, _c;
    try {
        const activeDomain = (_c = (_b = (_a = getMainCarrier().__SENTRY__) === null || _a === void 0 ? void 0 : _a.extensions) === null || _b === void 0 ? void 0 : _b.domain) === null || _c === void 0 ? void 0 : _c.active;
        // If there's no active domain, just return global hub
        if (!activeDomain) {
            return getHubFromCarrier(registry);
        }
        // If there's no hub on current domain, or it's an old API, assign a new one
        if (!hasHubOnCarrier(activeDomain) || getHubFromCarrier(activeDomain).isOlderThan(exports.API_VERSION)) {
            const registryHubTopStack = getHubFromCarrier(registry).getStackTop();
            setHubOnCarrier(activeDomain, new Hub(registryHubTopStack.client, scope_1.Scope.clone(registryHubTopStack.scope)));
        }
        // Return hub that lives on a domain
        return getHubFromCarrier(activeDomain);
    }
    catch (_Oo) {
        // Return hub that lives on a global object
        return getHubFromCarrier(registry);
    }
}
/**
 * This will tell whether a carrier has a hub on it or not
 * @param carrier object
 */
function hasHubOnCarrier(carrier) {
    return !!(carrier && carrier.__SENTRY__ && carrier.__SENTRY__.hub);
}
/**
 * This will create a new {@link Hub} and add to the passed object on
 * __SENTRY__.hub.
 * @param carrier object
 * @hidden
 */
function getHubFromCarrier(carrier) {
    if (carrier && carrier.__SENTRY__ && carrier.__SENTRY__.hub)
        return carrier.__SENTRY__.hub;
    carrier.__SENTRY__ = carrier.__SENTRY__ || {};
    carrier.__SENTRY__.hub = new Hub();
    return carrier.__SENTRY__.hub;
}
exports.getHubFromCarrier = getHubFromCarrier;
/**
 * This will set passed {@link Hub} on the passed object's __SENTRY__.hub attribute
 * @param carrier object
 * @param hub Hub
 * @returns A boolean indicating success or failure
 */
function setHubOnCarrier(carrier, hub) {
    if (!carrier)
        return false;
    carrier.__SENTRY__ = carrier.__SENTRY__ || {};
    carrier.__SENTRY__.hub = hub;
    return true;
}
exports.setHubOnCarrier = setHubOnCarrier;
//# sourceMappingURL=hub.js.map
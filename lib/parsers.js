Object.defineProperty(exports, "__esModule", { value: true });
const utils_1 = require("./utils");
const fs_1 = require("fs");
const lru_map_1 = require("lru_map");
const stacktrace = require("./stacktrace");
const DEFAULT_LINES_OF_CONTEXT = 7;
const FILE_CONTENT_CACHE = new lru_map_1.LRUMap(100);
/**
 * Resets the file cache. Exists for testing purposes.
 * @hidden
 */
function resetFileContentCache() {
    FILE_CONTENT_CACHE.clear();
}
exports.resetFileContentCache = resetFileContentCache;
/** JSDoc */
function getFunction(frame) {
    try {
        return frame.functionName || `${frame.typeName}.${frame.methodName || '<anonymous>'}`;
    }
    catch (e) {
        // This seems to happen sometimes when using 'use strict',
        // stemming from `getTypeName`.
        // [TypeError: Cannot read property 'constructor' of undefined]
        return '<anonymous>';
    }
}
const mainModule = `${(require.main && require.main.filename && utils_1.dirname(require.main.filename)) ||
    global.process.cwd()}/`;
/** JSDoc */
function getModule(filename, base) {
    if (!base) {
        base = mainModule;
    }
    // It's specifically a module
    const file = utils_1.basename(filename, '.js');
    filename = utils_1.dirname(filename);
    let n = filename.lastIndexOf('/node_modules/');
    if (n > -1) {
        // /node_modules/ is 14 chars
        return `${filename.substr(n + 14).replace(/\//g, '.')}:${file}`;
    }
    // Let's see if it's a part of the main module
    // To be a part of main module, it has to share the same base
    n = `${filename}/`.lastIndexOf(base, 0);
    if (n === 0) {
        let moduleName = filename.substr(base.length).replace(/\//g, '.');
        if (moduleName) {
            moduleName += ':';
        }
        moduleName += file;
        return moduleName;
    }
    return file;
}
/**
 * This function reads file contents and caches them in a global LRU cache.
 * Returns a Promise filepath => content array for all files that we were able to read.
 *
 * @param filenames Array of filepaths to read content from.
 */
function readSourceFiles(filenames) {
    // we're relying on filenames being de-duped already
    if (filenames.length === 0) {
        return utils_1.SyncPromise.resolve({});
    }
    return new utils_1.SyncPromise(resolve => {
        const sourceFiles = {};
        let count = 0;
        // tslint:disable-next-line: prefer-for-of
        for (let i = 0; i < filenames.length; i++) {
            const filename = filenames[i];
            const cache = FILE_CONTENT_CACHE.get(filename);
            // We have a cache hit
            if (cache !== undefined) {
                // If it's not null (which means we found a file and have a content)
                // we set the content and return it later.
                if (cache !== null) {
                    sourceFiles[filename] = cache;
                }
                count++;
                // In any case we want to skip here then since we have a content already or we couldn't
                // read the file and don't want to try again.
                if (count === filenames.length) {
                    resolve(sourceFiles);
                }
                continue;
            }
            fs_1.readFile(filename, (err, data) => {
                const content = err ? null : data.toString();
                sourceFiles[filename] = content;
                // We always want to set the cache, even to null which means there was an error reading the file.
                // We do not want to try to read the file again.
                FILE_CONTENT_CACHE.set(filename, content);
                count++;
                if (count === filenames.length) {
                    resolve(sourceFiles);
                }
            });
        }
    });
}
/**
 * @hidden
 */
function extractStackFromError(error) {
    const stack = stacktrace.parse(error);
    if (!stack) {
        return [];
    }
    return stack;
}
exports.extractStackFromError = extractStackFromError;
/**
 * @hidden
 */
function parseStack(stack, options) {
    const filesToRead = [];
    const linesOfContext = options && options.frameContextLines !== undefined ? options.frameContextLines : DEFAULT_LINES_OF_CONTEXT;
    const frames = stack.map(frame => {
        var _a;
        const parsedFrame = {
            colno: frame.columnNumber,
            filename: ((_a = frame.fileName) === null || _a === void 0 ? void 0 : _a.startsWith('file://')) ? frame.fileName.substr(7) : frame.fileName || '',
            function: getFunction(frame),
            lineno: frame.lineNumber,
        };
        const isInternal = frame.native ||
            (parsedFrame.filename &&
                !parsedFrame.filename.startsWith('/') &&
                !parsedFrame.filename.startsWith('.') &&
                parsedFrame.filename.indexOf(':\\') !== 1);
        // in_app is all that's not an internal Node function or a module within node_modules
        // note that isNative appears to return true even for node core libraries
        // see https://github.com/getsentry/raven-node/issues/176
        parsedFrame.in_app =
            !isInternal && parsedFrame.filename !== undefined && parsedFrame.filename.indexOf('node_modules/') === -1;
        // Extract a module name based on the filename
        if (parsedFrame.filename) {
            parsedFrame.module = getModule(parsedFrame.filename);
            if (!isInternal && linesOfContext > 0 && filesToRead.indexOf(parsedFrame.filename) === -1) {
                filesToRead.push(parsedFrame.filename);
            }
        }
        return parsedFrame;
    });
    // We do an early return if we do not want to fetch context liens
    if (linesOfContext <= 0) {
        return utils_1.SyncPromise.resolve(frames);
    }
    try {
        return addPrePostContext(filesToRead, frames, linesOfContext);
    }
    catch (_) {
        // This happens in electron for example where we are not able to read files from asar.
        // So it's fine, we recover be just returning all frames without pre/post context.
        return utils_1.SyncPromise.resolve(frames);
    }
}
exports.parseStack = parseStack;
/**
 * This function tries to read the source files + adding pre and post context (source code)
 * to a frame.
 * @param filesToRead string[] of filepaths
 * @param frames StackFrame[] containg all frames
 */
function addPrePostContext(filesToRead, frames, linesOfContext) {
    return new utils_1.SyncPromise(resolve => readSourceFiles(filesToRead).then(sourceFiles => {
        const result = frames.map(frame => {
            if (frame.filename && sourceFiles[frame.filename]) {
                try {
                    const lines = sourceFiles[frame.filename].split('\n');
                    utils_1.addContextToFrame(lines, frame, linesOfContext);
                }
                catch (e) {
                    // anomaly, being defensive in case
                    // unlikely to ever happen in practice but can definitely happen in theory
                }
            }
            return frame;
        });
        resolve(result);
    }));
}
/**
 * @hidden
 */
function getExceptionFromError(error, options) {
    const name = error.name || error.constructor.name;
    const stack = extractStackFromError(error);
    return new utils_1.SyncPromise(resolve => parseStack(stack, options).then(frames => {
        const result = {
            stacktrace: {
                frames: prepareFramesForEvent(frames),
            },
            type: name,
            value: error.message,
        };
        resolve(result);
    }));
}
exports.getExceptionFromError = getExceptionFromError;
/**
 * @hidden
 */
function parseError(error, options) {
    return new utils_1.SyncPromise(resolve => getExceptionFromError(error, options).then((exception) => {
        resolve({
            exception: {
                values: [exception],
            },
        });
    }));
}
exports.parseError = parseError;
/**
 * @hidden
 */
function prepareFramesForEvent(stack) {
    if (!stack || !stack.length) {
        return [];
    }
    let localStack = stack;
    const firstFrameFunction = localStack[0].function || '';
    if (firstFrameFunction.indexOf('captureMessage') !== -1 || firstFrameFunction.indexOf('captureException') !== -1) {
        localStack = localStack.slice(1);
    }
    // The frame where the crash happened, should be the last entry in the array
    return localStack.reverse();
}
exports.prepareFramesForEvent = prepareFramesForEvent;
//# sourceMappingURL=parsers.js.map